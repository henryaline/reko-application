<?php
/**
 * Template Name: Etusivu
 * The template for displaying homepage.
 *
 * This is the template that displays homepage by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>
	<div id="asdasdasd">
		<?php
			$rowcount = 0;
			if (have_rows('paivystaja')) :
				while (have_rows('paivystaja')) : the_row();
					$rowcount++;
					$nimi = get_sub_field('nimi');
					$titteli = get_sub_field('titteli');
					$puhelinnumero = get_sub_field('puhelinnumero');
					$sahkoposti = get_sub_field('sahkoposti');
					$kuva = get_sub_field('kuva');
					?>
					<input type="hidden" name="paivystys_nimi_<?php echo $rowcount; ?>" id="paivystys_nimi_<?php echo $rowcount; ?>" value="<?php echo $nimi; ?>">
					<input type="hidden" name="paivystys_titteli_<?php echo $rowcount; ?>" id="paivystys_titteli_<?php echo $rowcount; ?>" value="<?php echo $titteli; ?>">
					<input type="hidden" name="paivystys_puhelinnumero_<?php echo $rowcount; ?>" id="paivystys_puhelinnumero_<?php echo $rowcount; ?>" value="<?php echo $puhelinnumero; ?>">
					<input type="hidden" name="paivystys_sahkoposti_<?php echo $rowcount; ?>" id="paivystys_sahkoposti_<?php echo $rowcount; ?>" value="<?php echo $sahkoposti; ?>">
					<input type="hidden" name="paivystys_kuva_<?php echo $rowcount; ?>" id="paivystys_kuva_<?php echo $rowcount; ?>" value="<?php echo $kuva; ?>">
					<?php
				endwhile;
			else :
				// no rows found
			endif;
		?>
	</div>
	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<script>
	document.addEventListener("DOMContentLoaded", function(){
		var hdr = document.getElementById("fp-headerblock");
		var imgurl = document.getElementById("paivystys_kuva_0");
		hdr.style.backgroundImage = imgurl.value;
	});
</script>

<?php get_footer(); ?>
