import { Component, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { EventsService } from 'src/app/services/events.service';
import { ApiDataService } from '../services/api-data.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  @ViewChild('tabs', { static: true }) tabs: IonTabs;

  isLogged = false;
  private currentTab = null;
  asd = null;
  inCart = 0;

  constructor(
    private auth: AuthService,
    private api: ApiDataService,
    private event: EventsService
  ) {
    this.auth.check().then(() => { // has access token
      // return this.auth.load();
      this.isLogged = true;
    }).then(() => {
      console.log(this.isLogged);
      this.getCartCount();
    }).catch(err => {
      this.isLogged = false;
      console.log(err);
    });
    this.event.subscribe('user:loggedin', () => {
      // user and time are the same arguments passed in events.publish(user, time)
      // console.log('Paslaa');
    });
    this.event.subscribe('cart:updated', () => {
      console.log('cart:updated');
      this.getCartCount();
    });
  }

  getCartCount() {
    this.api.getCartItems().then((dt: any) => {
      this.inCart = dt.cartQuantity;
    });
  }
  tabsGonnaChange(tabsRef) {
    if (tabsRef.outlet.activatedView) {
      this.currentTab = tabsRef.outlet.activatedView.element;
    }
    this.auth.check().then(() => {
      this.getCartCount();
    });
  }
  ionViewDidEnter() {
    if (this.currentTab) {
      this.currentTab.dispatchEvent(new CustomEvent('ionViewDidEnter'));
    }
    this.auth.check().then(() => {
      this.getCartCount();
    });
  }
  ionViewWillEnter() {
    if (this.currentTab) {
      this.currentTab.dispatchEvent(new CustomEvent('ionViewWillEnter'));
    }
    this.auth.check().then(() => { // has access token
      // return this.auth.load();
      this.isLogged = true;
    }).then(() => {
      console.log(this.isLogged);
      this.getCartCount();
    }).catch(err => {
      this.isLogged = false;
      console.log(err);
    });
    
  }
  ionViewWillLeave() {
    if (this.currentTab) {
      this.currentTab.dispatchEvent(new CustomEvent('ionViewWillLeave'));
    }
    this.auth.check().then(() => {
      this.getCartCount();
    });
  }
  ionViewDidLeave() {
    if (this.currentTab) {
      this.currentTab.dispatchEvent(new CustomEvent('ionViewDidLeave'));
    }
    this.auth.check().then(() => {
      this.getCartCount();
    });
  }
}
