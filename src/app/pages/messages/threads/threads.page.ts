import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../../helpers/http-response';
import * as moment from 'moment';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.page.html',
  styleUrls: ['./threads.page.scss'],
})
export class ThreadsPage {

  threadData: any[] = [];
  threads: any[] = [];
  input = null;

  constructor(public http: HttpClient, private auth: AuthService, private api: ApiDataService,
              private loadCtrl: LoadingController, public alertCtrl: AlertController) {
    // this.data = this.auth.getCart().then(res => this.data = res);
    // this.auth.check().then(() => {
    //   this.getCartItems();
    // });
  }

  ionViewDidEnter() {
    this.auth.check().then(() => {
      this.getUserThreads();
    });
  }

  public getUserThreads() {
    return new Promise<void>((resolve, reject) => {
      this.api.getThreads().then((dt: any) => {
        this.threadData = dt;
        this.threads = dt.threads;

        for (const thr of this.threads) {
          console.log(thr);
          thr.date = moment(thr.updated_at).format('DD.MM.YYYY');
          thr.time = moment(thr.updated_at).format('HH:mm');
          // if (thr.messages[0].user.roles.includes('Tuottaja')) {
          if (thr.messages[0].user.role.some(role => role.name == 'Tuottaja')) {
            thr.sender = thr.messages[0].user.producer[0].name;
          } else if (thr.messages[0].user.role.some(role => role.name == 'Kauppias')) {
          // } else if (thr.messages[0].user.roles.includes('Kauppias')) {
            thr.sender = thr.messages[0].user.shops[0].name;
          } else {
            thr.sender = thr.messages[0].user.first_name + ' ' + thr.messages[0].user.last_name;
          }
        }
        
        console.log(this.threadData);
        console.log(this.threads);
        resolve();
      }).catch(err => reject(err));
    });
  }

}
