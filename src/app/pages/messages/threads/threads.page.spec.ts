import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ThreadsPage } from './threads.page';

describe('ThreadsPage', () => {
  let component: ThreadsPage;
  let fixture: ComponentFixture<ThreadsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ThreadsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
