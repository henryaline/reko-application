import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../../helpers/http-response';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-thread',
  templateUrl: './thread.page.html',
  styleUrls: ['./thread.page.scss'],
})
export class ThreadPage {

  threadData: any[] = [];
  threads: any[] = [];
  messages: any[] = [];
  input = null;
  thread = null;
  passedid = null;
  subject = '';
  data = null;
  details = null;
  public form: FormGroup = null;
  readData = { thread: 0 };


  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController,
    private route: ActivatedRoute,
    formBuilder: FormBuilder,
  ) {
    this.form = formBuilder.group({
      message: ['', Validators.required]
    });
    this.details = this.auth.getUserDetails().then(res => this.details = res);
  }

  ionViewDidEnter() {
    this.auth.check().then(() => {
      this.getThreadMessages();
    });
  }

  public getThreadMessages() {
    this.passedid = this.route.snapshot.paramMap.get('id');

    return new Promise<void>((resolve, reject) => {
      this.api.getThreads().then((dt: any) => {
        this.thread = dt.threads.find(i => i.id == this.passedid);
        this.messages = this.thread.messages;
        this.subject = this.thread.subject;
        if (this.thread.messages[0].user.role.some(role => role.name == 'Tuottaja')) {
          this.thread.sender = this.thread.messages[0].user.producer[0].name;
          this.thread.senderimg = this.thread.messages[0].user.producer[0].profile_image;
          this.thread.senderimgurl = this.thread.messages[0].user.producer[0].profilePictureUrl;
        } else if (this.thread.messages[0].user.role.some(role => role.name == 'Kauppias')) {
          this.thread.sender = this.thread.messages[0].user.shops[0].name;
          this.thread.senderimg = this.thread.messages[0].user.shops[0].profile_image;
          this.thread.senderimgurl = this.thread.messages[0].user.shops[0].profilePictureUrl;
        } else {
          this.thread.sender = this.thread.messages[0].user.first_name + ' ' + this.thread.messages[0].user.last_name;
        }
        for (const msg of this.messages) {
          msg.created = moment(msg.created_at).format('DD.MM.YY HH:mm');
          if (this.details.id != msg.user.id) {
            if (msg.user.role.some(role => role.name == 'Tuottaja')) {
              msg.sender = msg.user.producer[0].name;
              msg.senderimg = msg.user.producer[0].profile_image;
              msg.senderimgurl = msg.user.producer[0].profilePictureUrl;
            } else if (msg.user.role.some(role => role.name == 'Kauppias')) {
              msg.sender = msg.user.shops[0].name;
              msg.senderimg = msg.user.shops[0].profile_image;
              msg.senderimgurl = msg.user.shops[0].profilePictureUrl;
            } else {
              msg.sender = msg.user.first_name + ' ' + msg.user.last_name;
            }
          }
          // thr.date = moment(thr.updated_at).format('DD.MM.YYYY');
          // thr.time = moment(thr.updated_at).format('HH:mm');
          // // if (thr.messages[0].user.roles.includes('Tuottaja')) {
          // if (thr.messages[0].user.role.some(role => role.name == 'Tuottaja')) {
          //   thr.sender = thr.messages[0].user.producer[0].name;
          // } else if (thr.messages[0].user.role.some(role => role.name == 'Kauppias')) {
          //   // } else if (thr.messages[0].user.roles.includes('Kauppias')) {
          //   thr.sender = thr.messages[0].user.shops[0].name;
          // } else {
          //   thr.sender = thr.messages[0].user.first_name + ' ' + thr.messages[0].user.last_name;
          // }
        }

        console.log(this.thread);
        console.log(this.messages);
      }).then(() => {
        const threadid: number = +this.passedid;
        this.readData.thread = threadid;
        this.api.setMessageRead(this.readData);
      }).then(() => {
        console.log('Viestin tila päivitetty luetuksi.');
        resolve();
      }).catch(err => reject(err));
    });
  }


  async submit() {
    const threadid: number = +this.passedid;
    this.data = this.form.value;
    this.data.thread = threadid;
    console.log(this.data);

    const loader = await this.loadCtrl.create({
      message: 'Viestiä lähetetään',
    });
    await loader.present();

    this.auth.sendMsg(this.data).then(() => {
      return this.getThreadMessages();
    }).then(() => {
      this.form.reset();
    }).then(() => {
      loader.dismiss();
    }).catch(async err => {
      loader.dismiss();
      const response = new HttpResponse(err);
      const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
      await alrt.present();
      console.log('Tapahtui virhe: ');
      console.error(err);
    });
  }


}
