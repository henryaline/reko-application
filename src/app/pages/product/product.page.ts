import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Meta, Title } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { ApiDataService } from 'src/app/services/api-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { HttpResponse } from '../../helpers/http-response';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  isLogged = false;
  public form: FormGroup = null;
  details = null;
  userfavs = null;
  tiedot = null;
  passedid = null;
  maara = 1;
  data = null;
  action = '';
  isFaved = false;

  dt = { producer: 0, product: 0 };
  info = null;
  otherProducts = null;
  producer = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    formBuilder: FormBuilder,
    private auth: AuthService,
    public toastController: ToastController
  ) {
    this.form = formBuilder.group({
      maara: [1, Validators.required]
    });

    this.auth.check().then(() => {
      this.getDetails();
    }).then(() => {
      this.isLogged = true;
    }).catch(err => console.log(err));
  }

  ngOnInit() {
    this.passedid = this.route.snapshot.paramMap.get('id');
    this.api.haeData().then(data => {
      this.tiedot = data.products.find(i => i.id == this.passedid);
      if (!this.tiedot) {
        return new Promise<void>((resolve, reject) => {
          this.api.haeData(true).then(data2 => {
            this.tiedot = data2.products.find(i => i.id == this.passedid);
            resolve();
          });
        });
      } else {
        return Promise.resolve();
      }
    }).then(() => {
      this.getOtherProducts();
    });
  }

  getDetails() {
    return new Promise<void>((resolve, reject) => {
      this.api.getUserDetails().then((dt: any) => {
        const batchList: any[] = [];

        this.details = dt;
        this.userfavs = dt.favorites;
        for (const item of this.userfavs) {
          item.batch.forEach(p => batchList.push(p.id));
        }
        console.log(batchList);
        console.log(this.passedid);
        const iid: number = +this.passedid;
        if (batchList.includes(iid)) {
          this.isFaved = true;
        } else if (!batchList.includes(iid)) {
          this.isFaved = false;
        }
        console.log(this.isFaved);
        resolve();
      }).catch(err => reject(err));
    });
  }
  getOtherProducts() {
    const producerid: number = this.tiedot.product.producer[0].id;
    const productid: number = this.tiedot.product.id;
    this.dt.producer = producerid;
    this.dt.product = productid;

    this.api.getProductProducerOtherProducts(this.dt).then(d => {
      this.info = d;
      this.otherProducts = this.info.otherProducts;
    }).then(() => {
      console.log(this.info);
      console.log(this.otherProducts);
    });
  }

  public addOne() {
    this.maara = this.maara + 1;
  }

  public removeOne() {
    if (this.maara > 1) {
      this.maara = this.maara - 1;
    }
  }

  public addToCart() {
    const msg = 'Lisätään ostoskoriin...';
    this.action = 'tocart';
    this.submit(msg, this.action);
  }
  public addToFavs() {
    const msg = 'Lisätään suosikkeihin...';
    this.action = 'tofavs';
    this.submit(msg, this.action);
  }
  public rmFromFavs() {
    const msg = 'Lisätään suosikkeihin...';
    this.action = 'fromfavs';
    this.submit(msg, this.action);
  }


  async submit(msg, action) {
    const batch: number = +this.passedid;
    this.data = this.form.value;
    this.data.batch = batch;
    console.log(this.data);
    let apifunction = null;

    const loader = await this.loadCtrl.create({
      message: msg,
    });
    await loader.present();
    let newMsg = '';

    if (action == 'tofavs' ) {
      apifunction = this.auth.addFavs(this.data);
      newMsg = 'Tuote lisätty suosikkeihin.';
    } else if (action == 'fromfavs') {
      apifunction = this.auth.rmFromFavs(this.data);
      newMsg = 'Tuote poistettu suosikeista.';
    } else if (action == 'tocart') {
      apifunction = this.auth.addCart(this.data);
      newMsg = 'Tuote lisätty ostoskoriin.';
    }


    if (apifunction != null) {
      apifunction.then(() => {
        return this.getDetails();
      }).then(async () => {
        loader.dismiss();
        const toast = this.toastController.create({
          message: newMsg,
          duration: 3000,
          position: 'top',
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        });
        (await toast).present();
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ');
        console.error(err);
      });
    } else {
      loader.dismiss();
    }
  }

    // if (yesno === true) {
    //   const loader = await this.loadCtrl.create({
    //     message: 'Lisätään suosikkeihin...',
    //   });

    //   loader.present().then(() => {
    //     this.auth.addFavs(this.data).then(() => {
    //       loader.dismiss();
    //     }).catch(async err => {
    //       loader.dismiss();
    //       const response = new HttpResponse(err);
    //       const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
    //       await alrt.present();
    //       console.log('Tapahtui virhe: ');
    //       console.error(err);
    //     });
    //   });
    // } else {

    //   const loader = await this.loadCtrl.create({
    //     message: 'Lisätään ostoskoriin...',
    //   });

    //   loader.present().then(() => {
    //     this.auth.addCart(this.data).then(() => {
    //       loader.dismiss();
    //       this.navCtrl.navigateRoot('/tabs/cart');
    //     }).catch(async err => {
    //       loader.dismiss();
    //       const response = new HttpResponse(err);
    //       const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
    //       await alrt.present();
    //       console.log('Tapahtui virhe: ');
    //       console.error(err);
    //     });
    //   });
    // }

}
