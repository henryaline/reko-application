import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiDataService } from 'src/app/services/api-data.service';

@Component({
  selector: 'app-categorys-products',
  templateUrl: './categorys-products.page.html',
  styleUrls: ['./categorys-products.page.scss'],
})
export class CategorysProductsPage implements OnInit {

  hakusana = '';
  passedid = null;
  passedname = '';
  originalcategProducts: any[] = [];
  categProducts: any[] = [];
  categ = null;
  tiedot = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiDataService
  ) { }

  ngOnInit() {
    this.passedid = this.route.snapshot.paramMap.get('id');
    this.passedname = this.route.snapshot.paramMap.get('name');
    console.log(this.passedname);
    this.api.haeData().then(data => {
      console.log(data.products);
      this.categ = data.categories.find(i => i.id == this.passedid);
      // this.originalcategProducts = data.products.filter(product => product.product.category.find(cat => cat.id == this.passedid));
      this.originalcategProducts = data.products.filter(product => product.product.category.id == this.passedid);
      this.originalcategProducts.forEach(c => this.categProducts.push(c));
    }).then(() => {
      console.log(this.categProducts);
      console.log(this.categ);
      if (this.passedname != '' && this.passedname) {
        this.categProducts = this.originalcategProducts.filter(product => {
          return product.product.subcategory.name.toLowerCase() == this.passedname.toLowerCase();
        });
        setTimeout(() => {
          this.setClass(this.passedname);
        }, 300);
      }
    });
  }

  setClass(name: string) {
    const categspans = document.getElementsByClassName('categoryspan');
    for (let i = 0; i < categspans.length; i++) {
      const categspan = categspans[i] as HTMLElement;
      if (categspan.innerText == name) {
        categspan.classList.add('clicked');
      }
    }
  }

  toggleClass(e, val) {
    const classList = e.target.classList;
    const classes = e.target.className;
    const innertxt: string = e.target.innerText;
    // console.log(innertxt);
    classes.includes('clicked') ? classList.remove('clicked') : classList.add('clicked');
    const clickkeds = document.getElementsByClassName('clicked');
    if (clickkeds.length > 1) {
      for (let i = 0; i < clickkeds.length; i++) {
        const clicked = clickkeds[i] as HTMLElement;
        const text: string = clicked.innerText;
        // console.log(text);
        if (text == innertxt) {
        } else {
          clicked.classList.remove('clicked');
        }
      }
    }

    if (!classes.includes('clicked')) {
      const subcat = val;
      this.categProducts = this.originalcategProducts.filter(product => {
        return product.product.subcategory.name.toLowerCase() == val.toLowerCase();
      });
    } else {
      this.categProducts = this.originalcategProducts;
    }
    // this.productList = this.originalProductList.filter(pp => {
    //   return pp.product.category[0].name.indexOf(val) >= 0;
    //   // indexof laskee löytyykö annetusta stringistä kys string osaa, jos kyllä niin lisää listaan
    //   // jos kyllä, niin palauttaa numeron mistä kohtaa alkaa esim eka niin 0
    //   // jos ei, niin palauttaa negatiivisen luvun
    // }); // filteröin uuteen listaan OG listasta ne, joiden value on löytynyt.
    // // klikataan kategoriaa -> rullaa täyden listan läpi ja poimii ne jotka vastaa
    // // pitää: jos monta klikattu niin rullaisi uuteen listaan

    // if (classes.includes('clicked')) {
    //   val = '';
    //   this.productList = this.originalProductList;
    // }

    console.log(val);
  }
  
  // onSearchChange() {
  //   const hakusana = this.hakusana.toLowerCase();

  //   this.hautaList = this.originalHautaList.filter(hauta => {
  //     return hauta.sukunimi.toLowerCase().indexOf(hakusana) >= 0 ||
  //       hauta.etunimi.toLowerCase().indexOf(hakusana) >= 0 ||
  //       hauta.titteli.toLowerCase().indexOf(hakusana) >= 0 ||
  //       hauta.kuolinvuosi == hakusana ||
  //       hauta.syntymavuosi == hakusana;
  //   });
  // }

}
