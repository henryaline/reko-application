import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CategorysProductsPage } from './categorys-products.page';

describe('CategorysProductsPage', () => {
  let component: CategorysProductsPage;
  let fixture: ComponentFixture<CategorysProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorysProductsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CategorysProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
