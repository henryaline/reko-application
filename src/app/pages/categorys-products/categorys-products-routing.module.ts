import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategorysProductsPage } from './categorys-products.page';

const routes: Routes = [
  {
    path: '',
    component: CategorysProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategorysProductsPageRoutingModule {}
