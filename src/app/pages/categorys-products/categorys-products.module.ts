import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategorysProductsPageRoutingModule } from './categorys-products-routing.module';

import { CategorysProductsPage } from './categorys-products.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategorysProductsPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [CategorysProductsPage]
})
export class CategorysProductsPageModule {}
