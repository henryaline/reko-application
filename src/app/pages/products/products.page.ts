import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiDataService } from 'src/app/services/api-data.service';

@Component({
  selector: 'app-products',
  templateUrl: 'products.page.html',
  styleUrls: ['products.page.scss']
})
export class ProductsPage {

  hakusana = '';
  laktoositon = null;
  gluten = null;

  originalProductList: any[] = [];
  originalCategList: any[] = [];
  productList: any[] = [];
  categList: any[] = [];

  filteredList: any[] = [];


  constructor(public http: HttpClient, private api: ApiDataService) {
    // this.api.haeData().then(data => {
    //   this.originalProductList = data.products;
    //   this.originalProductList.forEach(p => this.productList.push(p));
    //   this.originalCategList = data.categories;
    //   this.originalCategList.forEach(p => this.categList.push(p));
    //   // debugger;
    //   console.log(this.productList);
    //   console.log(this.categList);
    // });
    // this.getData();
  }

  ionViewWillEnter() {
    this.getData();
  }

  public getData() {
    this.originalProductList = [];
    this.originalCategList = [];
    this.productList = [];
    this.categList = [];
    this.api.haeData().then(data => {
      this.originalProductList = data.products;
      this.originalProductList.forEach(p => this.productList.push(p));
      this.originalCategList = data.categories;
      this.originalCategList.forEach(p => this.categList.push(p));
      // debugger;
      console.log(this.productList);
      console.log(this.categList);
    });
  }

  public addOne() {
    console.log('klikked');
  }

  onSearchChange() {
    const hakusana = this.hakusana.toLowerCase();
    const gluten = this.gluten;
    const laktoositon = this.laktoositon;

    this.productList = this.originalProductList.filter(batch => {
      if (gluten && !laktoositon) {
        return batch.product.name.toLowerCase().indexOf(hakusana) >= 0 && batch.product.is_gluteine_free === 'kylla';
      } else if (!gluten && laktoositon) {
        return batch.product.name.toLowerCase().indexOf(hakusana) >= 0 && batch.product.is_lactose_free === 'kylla';
      } else if (gluten && laktoositon) {
        return batch.product.name.toLowerCase().indexOf(hakusana) >= 0
          && batch.product.is_lactose_free === 'kylla'
          && batch.product.is_gluteine_free === 'kylla';
      } else {
        return batch.product.name.toLowerCase().indexOf(hakusana) >= 0;
      }
    });
  }
  onGlutenChange() {
    const gluten = this.gluten;
    console.log('gluten ' + gluten);
    this.productList = this.productList.filter(batch => {
      return batch.product.is_gluteine_free === 'kylla';
    });
  }
  onLactosChange() {
    const laktoositon = this.laktoositon;
    console.log('lak' + laktoositon);
    if (laktoositon) {
      this.productList = this.productList.filter(batch => {
        return batch.product.is_lactose_free === 'kylla';
      });
    }
  }

  toggleClass(e, val) {
    const classList = e.target.classList;
    const classes = e.target.className;
    classes.includes('clicked') ? classList.remove('clicked') : classList.add('clicked');

    // this.kategorianNimi = data.hautausmaa.kategoriat.find(i => i.id == this.passedid);
    // this.originalkategHenkilot = data.haudat.filter(hauta => hauta.kategoriat.find(kat => kat.id == this.passedid));

    // this.originalkategHenkilot.forEach(h => this.kategHenkilot.push(h));

    this.productList = this.originalProductList.filter(pp => {
      return pp.product.category[0].name.indexOf(val) >= 0;
      // indexof laskee löytyykö annetusta stringistä kys string osaa, jos kyllä niin lisää listaan
      // jos kyllä, niin palauttaa numeron mistä kohtaa alkaa esim eka niin 0
      // jos ei, niin palauttaa negatiivisen luvun
    }); // filteröin uuteen listaan OG listasta ne, joiden value on löytynyt.
        // klikataan kategoriaa -> rullaa täyden listan läpi ja poimii ne jotka vastaa
        // pitää: jos monta klikattu niin rullaisi uuteen listaan

    if (classes.includes('clicked')) {
      val = '';
      this.productList = this.originalProductList;
    }

    console.log(val);

  }
}
