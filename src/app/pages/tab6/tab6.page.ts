import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { latLng } from 'leaflet';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../helpers/http-response';



@Component({
  selector: 'app-tab6',
  templateUrl: 'tab6.page.html',
  styleUrls: ['tab6.page.scss']
})
export class Tab6Page implements OnInit {

  markers = [];
  details = null;
  orderData: any[] = [];
  orders: any[] = [];
  producers: any[] = [];
  input = null;
  productCount = 0;

  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
    this.details = this.auth.getUserDetails().then( res => this.details = res);
    // console.log(this.details);
  }

  ngOnInit() {
    this.auth.check().then(() => {
      this.getUserOrders();
    });

    // this.markers.push({
    //   type: 'producer',
    //   position: latLng(61.505773, 21.802636),
    // });
    // this.markers.push({
    //   type: 'producer',
    //   position: latLng(61.478851, 21.732253),
    // });
    // this.markers.push({
    //   type: 'producer',
    //   position: latLng(61.509333, 21.945541),
    // });
  }

  public getUserOrders() {
    return new Promise<void>((resolve, reject) => {
      this.api.getTrackableOrders().then((dt: any) => {
        this.orderData = dt;
        this.orders = dt.orders;
        for (const order of this.orders) {
          order.markers = [];
          // order.itemcount = 0;
          // order.vastaanotettu = 0;
          // order.keratty = 0;
          // order.toimitettu = 0;
          // order.odottaa = 0;
          // for (const item of order.items) {
          //   order.itemcount++;
          //   item.number = order.itemcount;
          //   if (item.items_status == 'Vastaanotettu' || item.items_status == 'Kerätty' || item.items_status == 'Toimitettu') {
          //     order.vastaanotettu++;
          //   }
          //   if (item.items_status == 'Kerätty' || item.items_status == 'Toimitettu') {
          //     order.keratty++;
          //   }
          //   if (item.items_status == 'Toimitettu') {
          //     order.toimitettu++;
          //   }
          //   else {
          //     order.odottaa++;
          //   }
          //   let lat = null;
          //   let lon = null;
          //   if ((typeof item.producer.lat != 'undefined' && item.producer.lat) &&
          //       (typeof item.producer.lon != 'undefined' && item.producer.lon)) {
          //     order.markers.push({
          //       type: 'producer',
          //       position: latLng(
          //         item.producer.lat,
          //         item.producer.lon
          //       ),
          //       number: order.itemcount,
          //     });
          //   }
          // }
          order.markers.push({
            type: 'deliveryplace',
            position: latLng(
              order.delivery_place.lat,
              order.delivery_place.lon
            ),
          });
        }
        console.log(this.orderData);
        // console.log(this.orders);
        resolve();
      }).catch(err => reject(err));
    });
  }

  // for(const item of this.cartitems) {
  //   item.formattedprice = (item.price).toString().replace('.', ',');
  //   item.totalprice = (Math.round(item.quantity * item.price * 100) / 100).toString().replace('.', ',');
  // }

}
