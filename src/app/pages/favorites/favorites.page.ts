import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DatePipe } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { ApiDataService } from 'src/app/services/api-data.service';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpResponse } from 'src/app/helpers/http-response';

@Component({
  selector: 'app-favorites',
  templateUrl: 'favorites.page.html',
  styleUrls: ['favorites.page.scss']
})
export class FavoritesPage {

  data: any[] = [];
  favorites: any[] = [];
  favoritescomingtosale: any[] = [];
  favoritesnotsale: any[] = [];
  relatedFavorites: any[] = [];
  // relatedFavorites: { category: '', subcategory: ''};
  sortedArray: any[] = [];
  action: string;
  public form: FormGroup = null;
  dt = null;


  constructor(
    private auth: AuthService,
    public datepipe: DatePipe,
    private api: ApiDataService,
    private route: ActivatedRoute,
    private router: Router,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    formBuilder: FormBuilder,
    public toastController: ToastController
  ) { 
    this.form = formBuilder.group({
      maara: [1, Validators.required]
    });
  }
    // this.auth.check().then(() => {
    //   this.getUserFavorites();
    // });

  ionViewWillEnter() {
    this.auth.check().then(() => {
      this.getUserFavorites();
    });
  }

  public getUserFavorites() {
    // return new Promise((resolve, reject) => {
      this.api.getFavorites().then((dt: any) => {
        this.data = dt;
        this.favorites = dt.favorites;
        this.favoritescomingtosale = dt.favoritescomingtosale;
        this.favoritesnotsale = dt.favoritesNotInSale;
      }).then(() => {
        for (const fav of this.favorites) {
          fav.availability = 'yes';
          fav.endDate = moment(fav.batch[0].sale_end).format('DD/MM/YYYY');
        }
        for (const favv of this.favoritescomingtosale) {
          favv.availability = 'no';
          favv.startDate = moment(favv.batch[0].sale_begin).format('w');
          const cid = favv.category.id;
          const cname = favv.category.name;
          const scid = favv.subcategory.id;
          const scname = favv.subcategory.name;
          if (!this.relatedFavorites.find(e => e.scname === scname)) {
            this.relatedFavorites.push({ cid, cname, scid, scname });
          }
        }
        for (const favvv of this.favoritesnotsale) {
          favvv.availability = 'no';
          const cid = favvv.category.id;
          const cname = favvv.category.name;
          const scid = favvv.subcategory.id;
          const scname = favvv.subcategory.name;
          if (!this.relatedFavorites.find(e => e.scname === scname)) {
            this.relatedFavorites.push({ cid, cname, scid, scname });
          }
        }
      }).then(() => {
        console.log(this.favorites);
        console.log(this.favoritescomingtosale);
        console.log(this.favoritesnotsale);
        console.log(this.relatedFavorites);
        // resolve();
      }).catch(err => console.log(err));
      // }).catch(err => reject(err));
    // });
  }

  public rmFromFavs(input) {
    const msg = 'Lisätään suosikkeihin...';
    const inpt = input;
    this.action = 'fromfavs';
    console.log(inpt);
    this.submit(msg, this.action, inpt);
  }
  async submit(msg, action, inpt) {
    const batch: number = inpt;
    this.dt = this.form.value;
    this.dt.batch = batch;
    console.log(this.dt);
    let apifunction = null;

    const loader = await this.loadCtrl.create({
      message: msg,
    });
    await loader.present();
    let newMsg = '';

    if (action == 'fromfavs') {
      apifunction = this.auth.rmFromFavs(this.dt);
      newMsg = 'Tuote poistettu suosikeista.';
    }


    if (apifunction != null) {
      apifunction.then(() => {
        return this.getUserFavorites();
      }).then(async () => {
        loader.dismiss();
        const toast = this.toastController.create({
          message: newMsg,
          duration: 3000,
          position: 'top',
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        });
        (await toast).present();
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ');
        console.error(err);
      });
    } else {
      loader.dismiss();
    }
  }
}
