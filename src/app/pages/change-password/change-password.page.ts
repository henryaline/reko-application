import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { ApiDataService } from 'src/app/services/api-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpResponse } from '../../helpers/http-response';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  public form: FormGroup = null;
  details: any;

  constructor(
    formBuilder: FormBuilder,
    private loadCtrl: LoadingController,
    private auth: AuthService,
    private api: ApiDataService,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    public toastController: ToastController,
    public router: Router
  ) {
      this.form = formBuilder.group({
        old_password: ['', Validators.required],
        new_password: ['', Validators.required],
        new_password_c: ['', Validators.required],
      });
      this.auth.check().then(() => {
        this.getDetails();
      });
    }

  ngOnInit() {
  }

  getDetails() {
    return new Promise<void>((resolve, reject) => {
      this.api.getUserDetails().then((dt: any) => {
        this.details = dt;
        resolve();
      }).catch(err => reject(err));
    });
  }

  async submit() {
    const loader = await this.loadCtrl.create({
      message: 'Päivitetään...',
    });

    loader.present().then(() => {
      this.auth.changePassword(this.form.value).then(() => {
        loader.dismiss();
      }).then(async () => {
        const toast = this.toastController.create({
          message: 'Salasana vaihdettu',
          position: 'top',
          duration: 3000,
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        });
        (await toast).present();
      }).then(() => {
        this.navCtrl.pop();
        this.router.navigate(['/tabs/user']);
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ');
        console.error(err);
      });
    });
  }

}
