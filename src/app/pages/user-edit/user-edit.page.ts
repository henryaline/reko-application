import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { ApiDataService } from 'src/app/services/api-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpResponse } from '../../helpers/http-response';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.page.html',
  styleUrls: ['./user-edit.page.scss'],
})
export class UserEditPage implements OnInit {

  public form: FormGroup = null;
  details: any;

  constructor(
    formBuilder: FormBuilder,
    private loadCtrl: LoadingController,
    private auth: AuthService,
    private api: ApiDataService,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    public toastController: ToastController,
    public router: Router
  ) {
    this.form = formBuilder.group({
      email: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      postal_code: ['', Validators.required],
      city: ['', Validators.required]
    });
    this.auth.check().then(() => {
      this.getDetails();
    });
  }

  ngOnInit() {
  }

  getDetails() {
    return new Promise<void>((resolve, reject) => {
      this.api.getUserDetails().then((dt: any) => {
        this.details = dt;
        console.log(this.details);
        this.form.controls.email.setValue(this.details.email);
        this.form.controls.first_name.setValue(this.details.first_name);
        this.form.controls.last_name.setValue(this.details.last_name);
        this.form.controls.address.setValue(this.details.address);
        this.form.controls.postal_code.setValue(this.details.postal_code);
        this.form.controls.city.setValue(this.details.city);
        resolve();
      }).catch(err => reject(err));
    });
  }

  async submit() {
    const loader = await this.loadCtrl.create({
      message: 'Päivitetään...',
    });

    loader.present().then(() => {
      this.auth.updateUser(this.form.value).then(() => {
        loader.dismiss();
      }).then(async () => {
        const toast = this.toastController.create({
          message: 'Tiedot päivitetty',
          position: 'top',
          duration: 3000,
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        });
        (await toast).present();
      }).then(() => {
        this.navCtrl.pop();
        this.router.navigate(['/tabs/user']);
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ');
        console.error(err);
      });
    });
  }

}
