import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfirmnewpasswordPage } from './confirmnewpassword.page';
import { ExploreContainerComponentModule } from '../../../explore-container/explore-container.module';

describe('ConfirmnewpasswordPage', () => {
  let component: ConfirmnewpasswordPage;
  let fixture: ComponentFixture<ConfirmnewpasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmnewpasswordPage ],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmnewpasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
