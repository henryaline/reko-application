import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmnewpasswordPageRoutingModule } from './confirmnewpassword-routing.module';

import { ExploreContainerComponentModule } from '../../../explore-container/explore-container.module';

import { ConfirmnewpasswordPage } from './confirmnewpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmnewpasswordPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [ConfirmnewpasswordPage]
})
export class ConfirmnewpasswordPageModule {}
