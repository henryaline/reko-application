import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { LoadingController, ModalController, NavController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../../helpers/http-response';
import { EventsService } from 'src/app/services/events.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public form: FormGroup = null;

  constructor(
    private modalController: ModalController,
    private auth: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    formBuilder: FormBuilder,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private appData: AppDataService,
    private event: EventsService
  ) {
    this.form = formBuilder.group({
      // email: ['', Validators.compose([Validators.email, Validators.required])],
      phone_number: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    // this.appData.setScreen('Sisäänkirjautuminen', LoginPage.constructor.name);
  }

  async submit() {
    const loader = await this.loadCtrl.create({
      message: 'Kirjaudutaan...',
    });

    loader.present().then(() => {
      this.auth.login(this.form.value).then(() => {
        loader.dismiss();
        this.event.publish('user:loggedin');
        this.navCtrl.navigateRoot('');
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ' + err);
        // console.error(err);
      });
    });
  }

}
