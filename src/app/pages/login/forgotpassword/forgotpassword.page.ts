import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpResponse } from '../../../helpers/http-response';
import { EtusivuPage } from '../../etusivu/etusivu.page';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  @ViewChild('c0', { static: true }) c0;
  @ViewChild('c1', { static: true }) c1;
  public resetPasswordForm: FormGroup;
  public enterCodeForm: FormGroup;
  public phoneNumber = '';
  public alreadyHaveACode = false;

  constructor(private formBuilder: FormBuilder, private navCtrl: NavController, private apiProvider: ApiDataService,
              private loadingCtrl: LoadingController, private alertCtrl: AlertController, private toastCtrl: ToastController) {
    this.resetPasswordForm = this.formBuilder.group({
      phone_number: ['', Validators.required],
    });

    this.enterCodeForm = this.formBuilder.group({
      code: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(6)])],
      passwords: this.formBuilder.group({
        password: ['', Validators.required],
        passwordConfirm: ['', Validators.required],
      }, {
        validator(group) {
          return group.controls.password.value === group.controls.passwordConfirm.value ? null : { passwordDoesntMatch: true };
        }
      }),
    });
  }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.c0);
      if (this.c0) {
        this.c0.setFocus();
      } else if (this.c1) {
        this.c1.setFocus();
      }
    }, 650);
  }

  iHaveAlreadyTheCode() {
    this.alreadyHaveACode = true;
    this.enterCodeForm.addControl('phone_number', this.formBuilder.control('', Validators.required));
  }

  phoneNumberEntered() {
    return this.phoneNumber.length > 0;
  }

  clearPhoneNumber() {
    this.resetPasswordForm.setValue({ phone_number: '' });
    this.phoneNumber = '';
  }

  async sendForm() {
    const loader = await this.loadingCtrl.create({
      message: 'Ladataan...',
    });

    loader.present().then(() => {
      this.apiProvider.sendPasswordReset(this.resetPasswordForm.value.phone_number).then(() => {
        loader.dismiss();
        this.phoneNumber = this.resetPasswordForm.value.phone_number;
      }).catch(async (err) => {
        loader.dismiss();
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions())).present();
      });
    }, () => { });
  }

  async sendCodeForm() {
    const loader = await this.loadingCtrl.create({
      message: 'Ladataan...',
    });

    loader.present().then(() => {
      if (this.alreadyHaveACode) {
        this.phoneNumber = this.enterCodeForm.value.phone_number;
      }

      const args = {
        code: this.enterCodeForm.value.code,
        password: this.enterCodeForm.controls.passwords.value.password,
        phone_number: this.phoneNumber,
      };

      this.apiProvider.sendPasswordResetCode(args).then(async (data) => {
        loader.dismiss();
        (await this.toastCtrl.create({
          // message: 'Salasana vaihdettu.',
          // duration: 3000,
          // position: 'top',
          message: 'Salasana vaihdettu.',
          duration: 3000,
          position: 'top',
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        })).present();

        this.navCtrl.navigateBack(['/login']);
      }).catch(async (err) => {
        loader.dismiss();
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions())).present();
      });
    }, () => { });
  }
}
