import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterVerifyPageRoutingModule } from './register-verify-routing.module';
import { ExploreContainerComponentModule } from '../../../explore-container/explore-container.module';

import { RegisterVerifyPage } from './register-verify.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RegisterVerifyPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [RegisterVerifyPage]
})
export class RegisterVerifyPageModule {}
