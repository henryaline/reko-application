import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController, NavParams } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { REKOUser } from 'src/app/services/user';
import { EventsService } from 'src/app/services/events.service';
import { HttpResponse } from 'src/app/helpers/http-response';
import { createOfflineCompileUrlResolver } from '@angular/compiler';

@Component({
  selector: 'app-register-verify',
  templateUrl: './register-verify.page.html',
  styleUrls: ['./register-verify.page.scss'],
})
export class RegisterVerifyPage implements OnInit {
  @ViewChild('c1', { static: true }) c1;
  @ViewChild('c2', { static: true }) c2;
  @ViewChild('c3', { static: true }) c3;
  @ViewChild('c4', { static: true }) c4;
  public verifyForm: FormGroup;
  public sending = false;
  public dt = { access_token : null};

  constructor(private navCtrl: NavController, private formBuilder: FormBuilder, private auth: AuthService,
              private loadingCtrl: LoadingController, private alertCtrl: AlertController, private events: EventsService) {
    this.verifyForm = this.formBuilder.group({
      c1: ['', Validators.pattern('[0-9]{1}')],
      c2: ['', Validators.pattern('[0-9]{1}')],
      c3: ['', Validators.pattern('[0-9]{1}')],
      c4: ['', Validators.pattern('[0-9]{1}')],
    });

    this.verifyForm.valueChanges.subscribe((e) => {
      if (!this.verifyForm.valid) {
        return;
      }

      const data = [
        this.verifyForm.value.c1,
        this.verifyForm.value.c2,
        this.verifyForm.value.c3,
        this.verifyForm.value.c4,
      ].join('');

      if (data.length < 4) {
        if (this.verifyForm.value.c3.length > 0) {
          this.c4.setFocus();
        } else if (this.verifyForm.value.c2.length > 0) {
          this.c3.setFocus();
        } else if (this.verifyForm.value.c1.length > 0) {
          this.c2.setFocus();
        }
      }
    });
  }

  ngOnInit() {
    setTimeout(() => {
      if (this.c1) {
        this.c1.setFocus();
      }
    }, 250);
  }

  async sendVerifyCode() {
    const code = [
      this.verifyForm.value.c1,
      this.verifyForm.value.c2,
      this.verifyForm.value.c3,
      this.verifyForm.value.c4,
    ].join('');

    const loader = await this.loadingCtrl.create({ message: 'Vahvistetaan' });

    loader.present().then(() => {
      this.auth.verifyCandidateAsUser(code).then((data: any) => { // DATA = accestoken joka pitää tallentaa
        this.dt.access_token = data.access_token; // data palauttaa käyttäjän tiedot
        console.log(data);
        console.log(this.dt);
        this.auth.saveUserDetails(this.dt);
        loader.dismiss();
        this.navCtrl.navigateRoot('/tabs/etusivu');
      }).catch(async (err) => {
        loader.dismiss();
        console.log(err); // Palauttaa tämän vika
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions(err.error.error))).present();

        this.verifyForm.controls.c1.setValue('');
        this.verifyForm.controls.c2.setValue('');
        this.verifyForm.controls.c3.setValue('');
        this.verifyForm.controls.c4.setValue('');
        this.sending = false;
      });
    }, () => { });
  }
}
