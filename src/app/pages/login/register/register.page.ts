import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { LoadingController, ModalController, NavController, AlertController, ToastController } from '@ionic/angular';
import { HttpResponse } from '../../../helpers/http-response';
import { InputValidators } from '../../../helpers/input-validators';
import { ApiDataService } from 'src/app/services/api-data.service';
import { EtusivuPage } from '../../etusivu/etusivu.page';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  // public registrationDetailsForm: FormGroup = null;
  // public registrationDetails = null;
  // private candidateToken = null;
  // form: FormGroup;
  // public phoneNumberForm: FormGroup = null;
  public form: FormGroup = null;

  constructor(
    public navCtrl: NavController, public formBuilder: FormBuilder,
    public loadCtrl: LoadingController, public alertCtrl: AlertController, public auth: AuthService,
    private api: ApiDataService, private toastCtrl: ToastController
  ) {
    // this.phoneNumberForm = this.formBuilder.group({
    //   phone: ['', Validators.required],
    //   password: ['', Validators.required]
    // });
    this.form = formBuilder.group({
      phone_number: ['', Validators.required],
      password: ['', Validators.required],
    });
  }


  ngOnInit() {
  }

  async sendPhoneNumber() {
    const data = {
      phone_number: this.form.value.phone_number,
      password: this.form.value.password,
    };

    if (data.password.length === 0) {
      return;
    }

    const loader = await this.loadCtrl.create({ message: 'Ladataan...' });

    loader.present().then(() => {
      this.auth.createCandidateUser(data).then(() => {
        loader.dismiss();
        this.navCtrl.navigateForward(['/register-details']);
      }).catch(async (err) => {
        loader.dismiss();
        console.log(err);
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions(err.error.error))).present();
      });
    }, () => { });
  }

}
