import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { REKOUser } from 'src/app/services/user';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from 'src/app/helpers/http-response';

@Component({
  selector: 'app-register-details',
  templateUrl: './register-details.page.html',
  styleUrls: ['./register-details.page.scss'],
})
export class RegisterDetailsPage implements OnInit {
  public phoneNumberEntered = false;
  private loader;
  public personalDataForm: FormGroup = null;
  user: any;


  constructor(private formBuilder: FormBuilder, private auth: AuthService, private usr: REKOUser,
              private navCtrl: NavController, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
    // super();
    this.user = usr;
    console.log(this.user);

    if (this.user.phone_number.length > 0) {
      this.phoneNumberEntered = true;
    }
    // this.personalDataForm = this.formBuilder.group({
    //   first_name: [this.user.first_name, Validators.required],
    //   last_name: [this.user.last_name, Validators.required],
    //   phone_number: [this.user.phone_number, Validators.required],
    //   email: [this.user.email, Validators.required],
    //   address: [this.user.address,  Validators.required],
    //   postal_code: [this.user.postal_code,  Validators.required],
    //   city: [this.user.city,  Validators.required],
    // });
    this.personalDataForm = formBuilder.group({
      first_name: [this.user.first_name, Validators.required],
      last_name: [this.user.last_name, Validators.required],
      phone_number: [this.user.phone_number, Validators.required],
      email: [this.user.email, Validators.required],
      address: [this.user.address, Validators.required],
      postal_code: [this.user.postal_code, Validators.required],
      city: [this.user.city, Validators.required],
    });
  }

  ngOnInit() {
  }

  async sendPersonalData() {
    this.loader = await this.loadingCtrl.create({
      message: 'Tallennetaan...',
    });

    if (this.personalDataForm.value.email) {
      this.personalDataForm.value.email = this.personalDataForm.value.email.trim();
    }

    this.loader.present().then(() => {
      this.auth.saveCandidateUser(this.personalDataForm.value).then((data) => {
        this.loader.dismiss();
        console.log(this.personalDataForm.value);
        console.log(data);
        this.navCtrl.navigateForward(['/register-verify']);
      }).catch(async (err) => {
        this.loader.dismiss();
        console.log(err);
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions(err.error.error))).present();
      });
    });
  }

}
