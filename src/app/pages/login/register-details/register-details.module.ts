import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterDetailsPageRoutingModule } from './register-details-routing.module';
import { ExploreContainerComponentModule } from '../../../explore-container/explore-container.module';

import { RegisterDetailsPage } from './register-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterDetailsPageRoutingModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule
  ],
  declarations: [RegisterDetailsPage]
})
export class RegisterDetailsPageModule {}
