import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartPage } from './cart.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { CartPageRoutingModule } from './cart-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    // ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: CartPage }]),
    CartPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [CartPage]
})
export class CartPageModule {}
