import { ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { HttpResponse } from '../../helpers/http-response';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { latLng, marker, Icon } from 'leaflet';
import { NgZone } from '@angular/core';
import { EventsService } from 'src/app/services/events.service';
import { Plugins } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-cart',
  templateUrl: 'cart.page.html',
  styleUrls: ['cart.page.scss']
})
export class CartPage {

  cartdata: any;
  cartquantity: any;
  cartitems: any;
  batchinput = null;
  public form: FormGroup = null;
  data = null;
  originalDeliveryPoints: any[] = [];
  // deliveryPoints: any[] = [];
  deliveryPoints = null;
  markers = [];
  // producers = [];
  private handleIncomingData = null;
  nomessage = false;

  public ernor = false;
  public ernormessage = '';
  public ernormessage2 = '';


  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController,
    formBuilder: FormBuilder,
    public toastController: ToastController,
    public events: EventsService,
    // public zone: NgZone,
    public changeDetector: ChangeDetectorRef,
    private router: Router
  ) {
    this.api.haeData().then(data => {
      this.originalDeliveryPoints = data.deliveryplaces;
      this.deliveryPoints = data.deliveryplaces;
      // this.markers.push({
      //   type: 'deliverypoint',
      //   position: latLng(
      //     this.deliveryPoints.lat,
      //     this.deliveryPoints.lon
      //   )
      // });
      // this.originalDeliveryPoints.forEach(p => this.deliveryPoints.push(p));
      // debugger;
      // console.log(this.deliveryPoints);
    });
    this.form = formBuilder.group({
      information: [''],
      deliveryplace: ['', Validators.required],
      acceptance: [false, Validators.pattern('true')]
    });
    this.handleIncomingData = data => {
      this.handleIncomingDeepLinkData(data);
    };
  }


  ionViewWillEnter() {
    // this.events.subscribe('deeplink', data => console.log(data));
    this.events.subscribe('deeplink', this.handleIncomingData);
    // this.events.subscribe('updateScreen', () => {
    //   this.zone.run(() => {
    //     console.log('force update the screen');
    //   });
    // });
    this.auth.check().then(() => {
      return this.getCartItems();
    });
  }
  ionViewDidLeave() {
    this.events.unsubscribe('deeplink', this.handleIncomingData);
  }

  public getCartItems() {
    return new Promise<void>((resolve, reject) => {
      this.api.getCartItems().then((dt: any) => {
        // this.events.publish('updateScreen');
        this.cartdata = dt;
        this.cartquantity = dt.cartQuantity;
        this.cartitems = dt.cartitems;
        let count = 1;
        this.markers = [];
        const producers = [];
        for (const item of this.cartitems) {
          item.formattedprice = (item.price).toString().replace('.', ',');
          item.totalprice = (Math.round(item.quantity * item.price * 100) / 100).toString().replace('.', ',');
          item.count = count;

          // let asd = false;
          // if (!(item.attributes.producer in producers)) {
          //   producers[item.attributes.producer] = count;
          //   asd = true;
          // }
          // if ((typeof item.attributes.producer_lat != 'undefined' && item.attributes.producer_lat) &&
          //   (typeof item.attributes.producer_lon != 'undefined' && item.attributes.producer_lon)) {
          //   let numbr = 0;
          //   if (item.attributes.producer in producers) {
          //     numbr = producers[item.attributes.producer];
          //   } else {
          //     numbr = count;
          //   }
          //   item.count = numbr;
          //   this.markers.push({
          //     type: 'producer',
          //     number: numbr,
          //     position: latLng(
          //       item.attributes.producer_lat,
          //       item.attributes.producer_lon
          //     ),
          //   });
          // }
          // console.log(producers);
          // console.log(asd);
          // if (asd) {
          //   count++;
          // }
          count++;
        }
        // console.log(this.cartdata);
        // console.log(producers);
        resolve();
      // }).then(() => {
      //   for (const dp of this.deliveryPoints) {
      //     this.markers.push({
      //       type: 'deliverypoint',
      //       position: latLng(
      //         dp.lat,
      //         dp.lon
      //       )
      //     });
      //   }
      }).then(() => {
        // console.log('updateScreen');
        // console.log(this.markers);
        this.changeDetector.detectChanges();
      }).catch(err => reject(err));
    });
  }

  get deliveryplace() { return this.form.get('deliveryplace'); }
  get acceptance() { return this.form.get('acceptance'); }


  public addOne(input) {
    const type = 'addone';
    if (input.quantity >= 1) {
      input.quantity = input.quantity + 1;
      const msg = 'Lisätään ostoskoriin';
      this.submit(type, input, msg);
    }
  }
  public removeOne(input) {
    const type = 'removeone';
    if (input.quantity > 1) {
      input.quantity = input.quantity - 1;
      const msg = 'Vähennetään ostoskorista';
      this.submit(type, input, msg);
    } else {
      this.remove(input);
    }
  }
  public remove(input) {
    const type = 'removeall';
    if (input.quantity > 0) {
      input.quantity = input.quantity - input.quantity;
      const msg = 'Poistetaan ostoskorista';
      this.submit(type, input, msg);
    }
  }
  public checkout() {
    console.log(this.deliveryplace);
    if (this.deliveryplace.value == '') {
      this.ernor = true;
      this.ernormessage = 'Sinun tulee valita toimitussijainti!';
    } else {
      this.ernormessage = '';
    }
    if (!this.acceptance.value) {
      this.ernor = true;
      this.ernormessage2 = 'Sinun tulee hyväksyä palvelun ehdot!';
    } else {
      this.ernormessage2 = '';
    }
    if (this.acceptance.value && this.deliveryplace.value != '') {
      this.ernor = false;
    }
    const type = 'checkout';
    const msg = 'Tehdään tilausta';
    if (!this.ernor) { this.submit(type, null, msg); }
  }

  async createNewOrder(dt) {
    const loader = await this.loadCtrl.create({ message: 'Ladataan...' });

    loader.present().then(() => {
      this.api.checkout(dt).then((data: any) => { // Luodaan tilaus, palautetaan laskun maksamisen url
        loader.dismiss();
        Plugins.Browser.open({ // Avataan sovelluksen sisällä selain ja ohjataan maksamaan lasku
          // url: this.api.paymentUrl + '?sid=' + data.key,
          url: data.paymentUrl, // tilalle data.url joka luodaan kontrollerissa
          toolbarColor: '#880E4F'
        }).then(() => {
          // this.router.navigate(['/tab6']);
        }).catch(err => console.error(err));
      }).catch(async (err) => {
        loader.dismiss();
        const error = new HttpResponse(err);
        (await this.alertCtrl.create(error.getAlertControllerDefaultOptions())).present();
      });
    }).catch(err => console.error(err));
  }

  public getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }



  async submit(type, input, msg) {
    let apifunction = null;
    let messg = '';

    const loader = await this.loadCtrl.create({
      message: msg,
    });
    await loader.present();

    if (type == 'addone') {
      apifunction = this.auth.addCart({ batch: input.id, maara: 1 });
      messg = 'Yksi kappale lisätty.';
      // this.events.publish('cart:updated');
      // console.log('cart:updated');
    } else if (type == 'removeone') {
      apifunction = this.auth.rmOneFromCart({ batch: input.id });
      messg = 'Yksi kappale poistettu.';
      // this.events.publish('cart:updated');
      // console.log('cart:updated');
    } else if (type == 'removeall') {
      apifunction = this.auth.rmFromCart({ batch: input.id });
      messg = 'Tuote poistettu ostoskorista.';
      // this.events.publish('cart:updated');
      // console.log('cart:updated');
    } else if (type == 'checkout') {
      this.data = this.form.value;
      // this.data.deliveryplace = 1;
      // console.log(this.data);
      // apifunction = this.auth.checkout(this.data);
      apifunction = this.createNewOrder(this.data);
      messg = 'Tilaus luotu!';
      this.nomessage = true;

    }

    if (apifunction != null) {
      apifunction.then(() => {
        return this.getCartItems();
      }).then(async () => {
        loader.dismiss();
        const toast = this.toastController.create({
          message: messg,
          duration: 3000,
          position: 'top',
          cssClass: 'notification_alert',
          buttons: [
            {
              side: 'start',
              icon: 'notifications',
              text: ''
            }, {
              side: 'end',
              text: 'X',
              role: 'cancel',
            }
          ]
        });
        this.events.publish('cart:updated');
        if (!this.nomessage) { (await toast).present(); }
      }).catch(async err => {
        loader.dismiss();
        const response = new HttpResponse(err);
        const alrt = await this.alertCtrl.create(response.getAlertControllerDefaultOptions(err.error.error));
        await alrt.present();
        console.log('Tapahtui virhe: ');
        console.error(err);
        return this.getCartItems();
      });
    } else {
      loader.dismiss();
    }
  }

  async handleIncomingDeepLinkData(data: any) {
    if (data.status == 1) {
      console.log('maksu ok');

      // Ohjaa seurantasivulle ja heitä toasti että tilaus onnistunut
      this.router.navigate(['/tab6']);
      const toast = this.toastController.create({
        message: 'Maksutapahtuma onnistui ja tilauksesi on luotu!',
        duration: 3000,
        position: 'top',
        cssClass: 'notification_alert',
        buttons: [
          {
            side: 'start',
            icon: 'notifications',
            text: ''
          }, {
            side: 'end',
            text: 'X',
            role: 'cancel',
          }
        ]
      });
      (await toast).present();


      // const cc = new CreditCard(data.card);
      // this.auth.getUser().credit_card = cc;
      // this.card = cc;

      // this.showToast('Luottokortti tallennettu.');

      // if (this.subscriptionType) {
      //   let viewIndex = this.navCtrl.length() - 1;
      //   let params: any = { subscriptionType: this.subscriptionType, popOnly: this.popOnly, goto_wash: this.goto_wash };
      //   if (this.cars) params.cars = this.cars;

      //   this.navCtrl.push(VerifySubscriptionPage, params).then(() => {
      //     this.navCtrl.remove(viewIndex, 1);
      //   });
      // } else if (this.washType) {
      //   let viewIndex = this.navCtrl.length() - 1;
      //   let params: any = { washType: this.washType, popOnly: this.popOnly };
      //   if (this.car) params.car = this.car;

      //   this.navCtrl.push(VerifyPurchasePage, params).then(() => {
      //     this.navCtrl.remove(viewIndex, 1);
      //   });
      // }
    } else {
      console.log('maksu EI OLE ok');
      // Heitä errori laatikko jossa lukee että maksu hylätty / epäonnistui

      const msg = 'Maksutapahtuma ei onnistunut tai se hylättiin.';

    //   if (data.error) {
    //     if (data.is_custom_error) msg = data.error;
    //     else msg = this.appData.getStripeErrorMessage(data.error);
    //   }
      const alrt = await this.alertCtrl.create({
        // title: 'Virhe',
        message: msg,
        buttons: ['OK'],
      });
      await alrt.present();

      // this.alertCtrl.create({
      //   // title: 'Virhe',
      //   message: msg,
      //   buttons: ['OK'],
      // }).present();

    //   console.log(data, msg);
    }
  }
}
