import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { introPage } from './intro.page';

const routes: Routes = [
  {
    path: '',
    component: introPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class introPageRoutingModule {}
