import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { introPage } from './intro.page';

describe('introPage', () => {
  let component: introPage;
  let fixture: ComponentFixture<introPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ introPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(introPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
