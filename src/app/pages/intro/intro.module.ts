import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { introPageRoutingModule } from './intro-routing.module';

import { introPage } from './intro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    introPageRoutingModule
  ],
  declarations: [introPage]
})
export class introPageModule {}
