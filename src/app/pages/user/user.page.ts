import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage {

  isLogged = false;
  details: any;

  constructor(private auth: AuthService, private router: Router, private api: ApiDataService) { }

  ionViewWillEnter() {
    this.auth.check().then(() => {
      this.getDetails();
    }).then(() => {
      this.isLogged = true;
    }).catch(err => console.log(err));
  }

  getDetails() {
    return new Promise<void>((resolve, reject) => {
      this.api.getUserDetails().then((dt: any) => {
        this.details = dt;
        resolve();
      }).catch(err => reject(err));
    });
  }

  logout() {
    this.auth.logout().then(() => {
      this.router.navigate(['/welcome'], { replaceUrl: true });
    }).catch(err => {
      console.error(err);
      this.router.navigate(['/welcome'], { replaceUrl: true });
    });
  }
}
