import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtusivuPage } from './etusivu.page';

const routes: Routes = [
  {
    path: '',
    component: EtusivuPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtusivuPageRoutingModule {}
