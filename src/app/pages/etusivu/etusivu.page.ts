import { Component } from '@angular/core';
import { ApiDataService } from 'src/app/services/api-data.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-etusivu',
  templateUrl: 'etusivu.page.html',
  styleUrls: ['etusivu.page.scss']
})
export class EtusivuPage {

  isLogged = false;
  data: any;
  categdata: any;
  recents: any;
  popular: any;
  relatedFavorites: any;
  sortedArray: any;
  originalCategList: any[] = [];
  categList: any[] = [];

  constructor(private auth: AuthService, private api: ApiDataService) {
    this.getFPItems();
    this.auth.check().then(() => {
      this.isLogged = true;
    }).catch(err => {
      this.isLogged = false;
      console.log(err);
    });
  }

  public getFPItems() {
    // return new Promise((resolve, reject) => {
      this.api.getFrontPageItems().then((dt: any) => {
        this.data = dt;
        this.recents = dt.recent_added_products;
        this.popular = dt.most_bought_products;
        console.log(this.data);
        // resolve();
      }).catch(err => console.log(err));
      this.api.haeData().then(categdata => {
        this.originalCategList = categdata.categories;
        this.originalCategList.forEach(p => this.categList.push(p));
        // debugger;
        console.log(this.categList);
      });
      // }).catch(err => reject(err));
    // });
  }

  public fresh() {
    const recents = document.getElementsByClassName('tuoreimmat');
    if (recents.length > 0) {
      for (let i = 0; i < recents.length; i++) {
        const recent = recents[i] as HTMLElement;
        recent.style.display = 'block';
      }
    }
    const populars = document.getElementsByClassName('suosituimmat');
    if (populars.length > 0) {
      for (let ii = 0; ii < populars.length; ii++) {
        const popul = populars[ii] as HTMLElement;
        popul.style.display = 'none';
      }
    }
    const txt = document.getElementsByClassName('tuore');
    if (txt.length > 0) {
      txt[0].classList.add('active');
      txt[0].classList.remove('inactive');
    }
    const txtt = document.getElementsByClassName('suosittu');
    if (txtt.length > 0) {
      txtt[0].classList.remove('active');
      txtt[0].classList.add('inactive');
    }

  }
  public mostpopular() {
    const recents = document.getElementsByClassName('tuoreimmat');
    if (recents.length > 0) {
      for (let i = 0; i < recents.length; i++) {
        const recent = recents[i] as HTMLElement;
        recent.style.display = 'none';
      }
    }
    const populars = document.getElementsByClassName('suosituimmat');
    if (populars.length > 0) {
      for (let ii = 0; ii < populars.length; ii++) {
        const popul = populars[ii] as HTMLElement;
        popul.style.display = 'block';
      }
    }
    const txt = document.getElementsByClassName('tuore');
    if (txt.length > 0) {
      txt[0].classList.add('inactive');
      txt[0].classList.remove('active');
    }
    const txtt = document.getElementsByClassName('suosittu');
    if (txtt.length > 0) {
      txtt[0].classList.remove('inactive');
      txtt[0].classList.add('active');
    }
  }
}
