import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EtusivuPage } from './etusivu.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { EtusivuPageRoutingModule } from './etusivu-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    // ExploreContainerComponentModule,
    EtusivuPageRoutingModule
  ],
  declarations: [EtusivuPage]
})
export class EtusivuPageModule {}
