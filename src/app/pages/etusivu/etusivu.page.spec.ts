import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { EtusivuPage } from './etusivu.page';

describe('etusivuPage', () => {
  let component: EtusivuPage;
  let fixture: ComponentFixture<EtusivuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EtusivuPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(EtusivuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
