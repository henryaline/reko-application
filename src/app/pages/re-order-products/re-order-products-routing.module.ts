import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReOrderProductsPage } from './re-order-products.page';

const routes: Routes = [
  {
    path: '',
    component: ReOrderProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReOrderProductsPageRoutingModule {}
