import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { latLng } from 'leaflet';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../helpers/http-response';

@Component({
  selector: 'app-re-order-products',
  templateUrl: './re-order-products.page.html',
  styleUrls: ['./re-order-products.page.scss'],
})
export class ReOrderProductsPage implements OnInit {

  orderData: any;

  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.auth.check().then(() => {
      this.getOrder();
    });
  }

  public getOrder() {
    return new Promise((resolve, reject) => {
      this.api.getlastOrder().then((dt: any) => {
        this.orderData = dt.order;
        console.log(this.orderData);
        resolve();
      }).catch(err => reject(err));
    });
  }

}
