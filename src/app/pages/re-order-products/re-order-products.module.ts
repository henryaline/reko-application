import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReOrderProductsPageRoutingModule } from './re-order-products-routing.module';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { ReOrderProductsPage } from './re-order-products.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReOrderProductsPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [ReOrderProductsPage]
})
export class ReOrderProductsPageModule {}
