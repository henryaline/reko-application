import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReOrderProductsPage } from './re-order-products.page';

describe('ReOrderProductsPage', () => {
  let component: ReOrderProductsPage;
  let fixture: ComponentFixture<ReOrderProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReOrderProductsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReOrderProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
