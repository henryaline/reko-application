import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProducersPage } from './producers.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { ProducersPageRoutingModule } from './producers-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    // ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: ProducersPage }]),
    ProducersPageRoutingModule,
  ],
  declarations: [ProducersPage]
})
export class ProducersPageModule {}
