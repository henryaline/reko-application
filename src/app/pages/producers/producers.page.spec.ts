import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { ProducersPage } from './producers.page';

describe('ProducersPage', () => {
  let component: ProducersPage;
  let fixture: ComponentFixture<ProducersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProducersPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ProducersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
