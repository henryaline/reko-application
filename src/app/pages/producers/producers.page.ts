import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiDataService } from 'src/app/services/api-data.service';

@Component({
  selector: 'app-producers',
  templateUrl: 'producers.page.html',
  styleUrls: ['producers.page.scss']
})
export class ProducersPage {

  hakusana = '';

  originalProducerList: any[] = [];
  producerList: any[] = [];

  constructor(public http: HttpClient, private api: ApiDataService) {
    this.api.haeData().then(data => {
      this.originalProducerList = data.producers;
      this.originalProducerList.forEach(p => this.producerList.push(p));
      console.log(data.producers);
      // debugger;
    });
  }

  onSearchChange() {
    const hakusana = this.hakusana.toLowerCase();

    this.producerList = this.originalProducerList.filter(producer => {
      return producer.name.toLowerCase().indexOf(hakusana) >= 0;
    });
  }

}
