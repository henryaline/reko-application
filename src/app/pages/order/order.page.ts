import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../helpers/http-response';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage {

  orderData: any[] = [];
  orders: any[] = [];
  items: any[] = [];
  input = null;
  thread = null;
  passedid = null;
  subject = '';
  data = null;
  details = null;
  order = null;

  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController,
    private route: ActivatedRoute,
  ) { }

  ionViewDidEnter() {
    this.auth.check().then(() => {
      this.getOrderDetails();
    });
  }

  public getOrderDetails() {
    this.passedid = this.route.snapshot.paramMap.get('id');

    this.api.getAllOrders().then((dt: any) => {
      this.order = dt.orders.find(i => i.id == this.passedid);
      this.items = this.order.items;
      console.log(this.order);
      console.log(this.items);
    }).catch(err => console.error(err));
  }

}
