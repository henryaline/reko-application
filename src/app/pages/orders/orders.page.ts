import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ApiDataService } from 'src/app/services/api-data.service';
import { HttpClient } from '@angular/common/http';
import { LoadingController, AlertController } from '@ionic/angular';
import { HttpResponse } from '../../helpers/http-response';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage {

  orderData: any[] = [];
  orders: any[] = [];
  input = null;

  constructor(
    public http: HttpClient,
    private auth: AuthService,
    private api: ApiDataService,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController
  ) { }

  ionViewDidEnter() {
    this.auth.check().then(() => {
      this.getUserOrders();
    });
  }

  public getUserOrders() {
    return new Promise((resolve, reject) => {
      this.api.getAllOrders().then((dt: any) => {
        this.orderData = dt;
        this.orders = dt.orders;
        console.log(this.orderData);
        console.log(this.orders);
        resolve();
      }).catch(err => reject(err));
    });
  }

}
