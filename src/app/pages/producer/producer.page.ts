import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Meta, Title } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { ApiDataService } from 'src/app/services/api-data.service';
// import { AuthService } from 'src/app/services/auth.service';
import * as moment from 'moment';
import { latLng } from 'leaflet';


@Component({
  selector: 'app-producer',
  templateUrl: './producer.page.html',
  styleUrls: ['./producer.page.scss'],
})
export class ProducerPage implements OnInit {

  tiedot = null;
  passedid = null;
  info = null;
  // data = null;
  producer = null;
  data = { producer: 0 };
  markers = [];


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiDataService,
    public navCtrl: NavController,
    // private auth: AuthService
  ) { }

  ngOnInit() {
    this.passedid = this.route.snapshot.paramMap.get('id');
    // this.auth.check().then(() => {
    //   this.getDetails();
    // });
    this.getDetails();
    // this.api.haeData().then(data => {
    //   this.tiedot = data.producers.find(i => i.id == this.passedid);
    // }).then(() => {
    //   for (const p of this.tiedot.product) {
    //     if (p.batch.length == 0) {
    //       p.hasCurrentSales = 'inactive';
    //     } else {
    //       p.hasCurrentSales = 'active';
    //     }
    //   }
    //   // console.log(this.tiedot);
    //   this.getDetails();
    // });
  }

  getDetails() {
    const id: number = +this.passedid;
    this.data.producer = id;

    this.api.getProducerProducts(this.data).then(d => {
      this.info = d;
      this.producer = this.info.producer;
      console.log(this.info);
    }).then(() => {
      // for (const p of this.producer.product) {
      //   if (p.batch.length == 0) {
      //     p.hasCurrentSales = 'inactive';
      //   } else {
      //     p.hasCurrentSales = 'active';
      //   }
      // }
      for (const favv of this.info.productsComingToSale) {
        favv.startDate = moment(favv.batch[0].sale_begin).startOf('isoWeek').format('w');
      }
      console.log(this.producer);
    }).then(() => {
      this.markers.push({
        type: 'single_producer',
        position: latLng(this.producer.lat, this.producer.lon),
      });
    });
  }

}
