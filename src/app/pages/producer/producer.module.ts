import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProducerPageRoutingModule } from './producer-routing.module';

import { ProducerPage } from './producer.page';

import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProducerPageRoutingModule,
    ComponentsModule,
    ExploreContainerComponentModule
  ],
  declarations: [ProducerPage]
})
export class ProducerPageModule {}
