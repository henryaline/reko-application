import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { ApiDataService } from './services/api-data.service';
import { AppDataService } from './services/app-data.service';
import { EventsService } from './services/events.service';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  // public isReady = false;
  public isLogged = false;
  public userdata = null;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    private router: Router,
    private apid: ApiDataService,
    private appData: AppDataService,
    private events: EventsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initDeeplinks();
    });

    try {
      this.auth.check().then(() => { // has access token
        return this.auth.load();
      }).then(() => {
        return this.auth.getUserDetails();
      }).then((response) => {
        this.isLogged = true;
        this.userdata = response;
        // this.isReady = true;

        // this.router.navigate(['/tabs/tab6'], { replaceUrl: true });
        // .finally(() => {
        //   if (Capacitor.isPluginAvailable('SplashScreen')) {
        //     SplashScreen.hide();
        //   }
        // });
      }).then(() => {
        console.log(this.isLogged);
        Plugins.PushNotifications.requestPermission().then(permission => {
          if (permission.granted) {
            Plugins.PushNotifications.addListener('registration', token => {
              this.updateDeviceData(token.value, this.userdata.id);
            });
          }
        }).catch(err => console.error(err));
      }).catch(err => {
        this.isLogged = false;
        // this.isReady = true;
        // this.router.navigate(['/intro'], { replaceUrl: true });
        console.log(err);
        //   .finally(() => {
        //   if (Capacitor.isPluginAvailable('SplashScreen')) {
        //     SplashScreen.hide();
        //   }
        // });
      });
    } catch (e) {
      console.error(e);
    }
  }

  updateDeviceData(token, uid) {
    Plugins.Device.getInfo().then(info => {
      // const uid = this.auth.isAuthenticated() ? this.auth.getUser().id : '';

      const details = {
        token,
        uuid: info.uuid,
        device: info.manufacturer + ' / ' + info.model,
        app_version: info.appVersion,
        user_id: uid,
      };

      let updt: Promise<any> = null;

      if (uid) {
        updt = this.apid.updateDeviceData(details);
      } else {
        updt = this.apid.registerDeviceData(details);
      }

      updt.then(() => {
        this.appData.setToStorage('deviceInfo', details);
      }).catch(err => console.error(err));
    }).catch(err => {
      console.log('Virhe devicedatassa');
      console.error(err);
    });
  }

  logout() {
    this.auth.logout().then(() => {
      this.router.navigate(['/welcome'], { replaceUrl: true });
    }).catch(err => {
      console.error(err);
      this.router.navigate(['/welcome'], { replaceUrl: true });
    });
  }

  initDeeplinks() {
    Plugins.App.addListener('appUrlOpen', data => {
      console.log('Incoming data from deeplink', data.url, this.parseDeeplink(data.url));
      this.events.publish('deeplink', this.parseDeeplink(data.url));
      Plugins.Browser.close().then(() => { }).catch(err => console.error(err));
    });
  }
  parseDeeplink(url: string) {
    if (url.indexOf('?') === -1) {
      return {};
    }
    const data = url.substr(url.indexOf('?') + 1);
    if (!data) {
      return {};
    }
    try {
      return JSON.parse(atob(data));
    } catch (e) {
      return data;
    }
  }
}
