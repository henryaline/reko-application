export class InputValidators {
    public static email(control): any {
        if (control.value == null) { return null; }
        else if (control.value.length == 0) { return null; }

        const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (emailPattern.test(control.value)) {
            return null;
        } else {
            return { invalidEmailAddress: true };
        }
    }

    public static requiredEmail(control) {
        if (control.value == null) { return null; }

        if (control.value.length == 0) { return { emailAddressRequired: true }; }
        else { return this.email(control); }
    }

    public static strongPassword(control) {
        const pw = control.value;

        if (pw.search(/[0-9]/) >= 0 && pw.search(/[a-z]/) >= 0 && pw.search(/[A-Z]/) >= 0) {
            return null;
        } else {
            return { notEnoughStrongPassword: true };
        }
    }

    public static phoneNumber(control) {
        const val = control.value;

        if (['0', '+'].indexOf(val.substr(0, 1)) < 0) { return { phoneNumberNotValid: true }; }
        if (val.length < 6) { return { phoneNumberNotValid: true } }

        return null;
    }

    public static phoneNumberWithoutLength(control) {
        const val = control.value;

        if (['0', '+'].indexOf(val.substr(0, 1)) < 0) { return { phoneNumberNotValid: true }; }
        if (val.length < 1) { return { phoneNumberNotValid: true } }

        return null;
    }

    public static month(control) {
        if (control.value == null || control.value.length == 0) { return null; }

        if (control.value.search(/(^[1-9]{1}$|^1[0-2]{1}$)/) >= 0) {
            return null;
        } else {
            return { invalidMonth: true };
        }
    }

    public static futureYear(control) {
        if (control.value == null || control.value.length == 0) { return null; }

        const i = parseInt(control.value);
        const d = new Date();
        const y = parseInt(d.getFullYear().toString().substr(0, 2)) * 100 + i;

        if (d.getFullYear() <= y) { return null; }
        else { return { invalidYear: true }; }
    }
}
