export class HttpResponse {
    private responseJson: any = null;

    constructor(public response: any) {
        // console.log(this.response);
        // console.log('plööö');
        this.responseJson = typeof (this.response.json) == 'function' ? this.response.json() : {};
    }

    isNull() {
        return this.response == null;
    }

    isError() {
        return !this.isNull() && (typeof (this.responseJson.error) != 'undefined' || this.isNetworkError() || this.isTimeoutError());
    }

    isInfo() {
        return !this.isNull() && typeof (this.responseJson.info) != 'undefined';
    }

    isNetworkError() {
        return !this.isNull() && typeof (this.response.status) != 'undefined' && this.response.status == 0;
    }

    isTimeoutError() {
        return !this.isNull() && typeof (this.response.status) != 'undefined' && this.response.status == 408;
    }

    getMessage(defaultMessage?: string) {
        if (this.isInfo()) { return this.responseJson.info; }
        else if (defaultMessage) { return defaultMessage; }
        else { return ''; }
    }

    getErrorMessage(defaultMessage?: string) {
        if  (this.isError() && typeof (this.responseJson.error) != 'undefined') { return this.responseJson.error; }
        else if (this.isTimeoutError()) { return 'Yhteys palvelimeen kesti liian kauan. Tarkista verkkoyhteys.'; }
        else if (this.isNetworkError()) { return 'Palvelimeen ei saada yhteyttä. Tarkista verkkoyhteys.'; }
        else { return defaultMessage ? defaultMessage : ''; }
    }

    getAlertControllerDefaultOptions(defaultMessage?: string) {
        return {
            title: 'Huomio',
            message: this.getErrorMessage(defaultMessage ? defaultMessage : 'Tuntematon virhe. Yritä uudelleen.'),
            buttons: ['OK'],
        };
    }

}
