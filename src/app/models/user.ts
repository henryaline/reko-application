import { AppDataService } from '../services/app-data.service';

export class User {
    // tslint:disable-next-line:variable-name
    public id = null; // tslint:disable-next-line:variable-name
    // tslint:disable-next-line:variable-name;
    // tslint:disable-next-line:variable-name
    public phone_number = ''; // tslint:disable-next-line:variable-name
    // tslint:disable-next-line:variable-name
    public first_name = '';
    // tslint:disable-next-line:variable-name
    public last_name = '';
    public email = '';
    public address = '';
    // tslint:disable-next-line:variable-name
    public postal_code = '';
    // tslint:disable-next-line:variable-name
    public city = '';


    isRegisteredUser() {
        return this.id !== 0;
    }
}