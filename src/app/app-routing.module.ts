import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tabs/etusivu',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then(m => m.introPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./pages/login/welcome/welcome.module').then( m => m.welcomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/login/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'elements',
    loadChildren: () => import('./elements/elements.module').then( m => m.ElementsPageModule)
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./pages/login/forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  {
    path: 'confirmnewpassword',
    loadChildren: () => import('./pages/login/confirmnewpassword/confirmnewpassword.module').then( m => m.ConfirmnewpasswordPageModule)
  },
  {
    path: 'product/:id',
    loadChildren: () => import('./pages/product/product.module').then( m => m.ProductPageModule)
  },
  {
    path: 'producer/:id',
    loadChildren: () => import('./pages/producer/producer.module').then( m => m.ProducerPageModule)
  },
  // {
  //   path: 'threads',
  //   loadChildren: () => import('./pages/messages/threads/threads.module').then( m => m.ThreadsPageModule)
  // },
  {
    path: 'thread/:id',
    loadChildren: () => import('./pages/messages/thread/thread.module').then( m => m.ThreadPageModule)
  },
  {
    path: 'order/:id',
    loadChildren: () => import('./pages/order/order.module').then( m => m.OrderPageModule)
  },
  {
    path: 'tab6',
    loadChildren: () => import('./pages/tab6/tab6.module').then(m => m.Tab6PageModule)
  },
  {
    path: 'threads',
    loadChildren: () => import('./pages/messages/threads/threads.module').then(m => m.ThreadsPageModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('./pages/orders/orders.module').then(m => m.OrdersPageModule)
  },
  {
    path: 'categorys-products/:id',
    loadChildren: () => import('./pages/categorys-products/categorys-products.module').then( m => m.CategorysProductsPageModule)
  },
  {
    path: 'categorys-products/:id/:name',
    loadChildren: () => import('./pages/categorys-products/categorys-products.module').then(m => m.CategorysProductsPageModule)
  },
  {
    path: 'user-edit',
    loadChildren: () => import('./pages/user-edit/user-edit.module').then( m => m.UserEditPageModule)
  },
  {
    path: 'terms-of-service',
    loadChildren: () => import('./pages/terms-of-service/terms-of-service.module').then( m => m.TermsOfServicePageModule)
  },
  {
    path: 're-order-products',
    loadChildren: () => import('./pages/re-order-products/re-order-products.module').then( m => m.ReOrderProductsPageModule)
  },
  {
    path: 'register-details',
    loadChildren: () => import('./pages/login/register-details/register-details.module').then( m => m.RegisterDetailsPageModule)
  },
  {
    path: 'register-verify',
    loadChildren: () => import('./pages/login/register-verify/register-verify.module').then( m => m.RegisterVerifyPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./pages/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  // {
  //   path: 'user',
  //   loadChildren: () => import('./pages/user/user.module').then( m => m.UserPageModule)
  // },
  // {
  //   path: 'orders',
  //   loadChildren: () => import('./pages/orders/orders.module').then( m => m.OrdersPageModule)
  // },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
