import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { MapComponent } from './map/map.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule
    ],
    exports: [
        MapComponent
    ],
    declarations: [MapComponent]
})
export class ComponentsModule { }
