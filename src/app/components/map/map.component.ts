import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { map, tileLayer, latLng, marker, latLngBounds, Icon, divIcon, layerGroup } from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, OnChanges {

  @Input() markers: any[];
  @ViewChild('map', { static: true }) mapElement: ElementRef;

  private map;
  private markerLayer;
  private producericon: Icon = null;
  private tstIcon: divIcon = null;
  private deliverypointicon: Icon = null;
  private smallIcon = Icon.extend({
    options: {
      iconSize: [22, 33],
      iconAnchor: [7, 20],
      popupAnchor: [0, -25]
    }
  });

  constructor() {
    this.producericon = new this.smallIcon({ iconUrl: '/assets/icon/pinpoint.png' });
    this.deliverypointicon = new this.smallIcon({ iconUrl: '/assets/icon/shop_pinpoint.png' });
  }

  ngOnChanges() {
    if (this.markerLayer) {
      this.addMarkers();
    }
  }
  ngOnInit() {
    setTimeout(() => {
      this.mapInit();
    }, 400 );
  }
  mapInit() {
    this.map = map(this.mapElement.nativeElement).setView(latLng(61.4867545, 21.7961847), 15);

    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      // attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
    this.markerLayer = layerGroup();
    this.markerLayer.addTo(this.map);
    this.addMarkers();
    this.map.fitBounds(latLngBounds(this.markers.map((m: any) => m.position)));
  }

  addMarkers() {
    this.markerLayer.clearLayers();
    for (const mapMarker of this.markers) {
      if ((mapMarker as any).type == 'producer') {
        marker(
          (mapMarker as any).position,
          {
            icon: new divIcon({
              iconSize: [22, 33],
              className: 'producer-icon',
              html: '<span style="background-image:url(../assets/icon/pinpoint.png);background-repeat:no-repeat;background-position:center center;background-color:transparent;background-size:contain;width:20px;height:30px;position:absolute;top:0px;text-align:center;line-height:26px;font-size:12px;font-weight:bold;color:#ffffff;">' + (mapMarker as any).number + '</span>'
            })
          }
        ).addTo(this.markerLayer);
      } else if ((mapMarker as any).type == 'single_producer') {
        marker((mapMarker as any).position, { icon: this.producericon }).addTo(this.markerLayer);
      } else {
        marker((mapMarker as any).position, { icon: this.deliverypointicon }).addTo(this.markerLayer);
      }
    }
  }
}
