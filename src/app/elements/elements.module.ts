import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElementsPageRoutingModule } from './elements-routing.module';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';


import { ElementsPage } from './elements.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElementsPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [ElementsPage]
})
export class ElementsPageModule {}
