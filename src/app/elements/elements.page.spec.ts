import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { ElementsPage } from './elements.page';

describe('ElementsPage', () => {
  let component: ElementsPage;
  let fixture: ComponentFixture<ElementsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementsPage ],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ElementsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
