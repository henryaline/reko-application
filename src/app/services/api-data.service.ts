import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, timeout, catchError } from 'rxjs/operators';
import { REKOUser } from 'src/app/services/user';

export enum SearchType {
  all = ''
}

@Injectable({
  providedIn: 'root'
})
export class ApiDataService {
  // private apiUrl = 'http://127.0.0.1:8000/api/reko';
  private apiUrl = 'https://reko.dev.aline.fi/api/reko';
  // private apiUrl = 'http://reko:8888/api/reko';
  public reko: any = null;
  public producerdata: any = null;
  private accessToken = null;
  private lastDataFetch = 0;

  constructor(private http: HttpClient, @Inject(REKOUser) private user: REKOUser) { }

  // API Datan haku
  haeData(force?): Promise<any> {
    if (this.lastDataFetch > Date.now() - 60000 && !force && this.reko) {
      return Promise.resolve(this.reko);
    }
    return this.get(this.apiUrl).then(data => {
      this.reko = data;
      return Promise.resolve(this.reko);
    }).catch($err => Promise.reject($err));
  }

  // Haetaan etusivun tuotteet
  public getFrontPageItems() {
    return this.get(this.apiUrl + '/front_page', {});
  }

  // Haetaan tuottajan tiedot sekä tuotteet
  public getProducerProducts(data) {
    return this.get(this.apiUrl + '/producer/products', data);
  }
  // Haetaan tuotteen tuottajan toiset tuotteet
  public getProductProducerOtherProducts(data) {
    return this.get(this.apiUrl + '/product/producer_other_products', data);
  }

  // Ostoskorin tapahtumat
  public getCartItemsCount() {
    return this.get(this.apiUrl + '/cart_count', {}, true);
  }
  public getCartItems() {
    return this.get(this.apiUrl + '/cart', {}, true);
  }
  public addToCart(data) {
    return this.post(this.apiUrl + '/cart/add', data, true);
  }
  public removeOneFromCart(data) {
    return this.post(this.apiUrl + '/cart/remove_one', data, true);
  }
  public removeFromCart(data) {
    return this.post(this.apiUrl + '/cart/remove_all', data, true);
  }
  // Ostoskorin tapahtumat päättyy

  // Checkout | Uusi tilaus
  // public payment() {
  //   return this.get(this.apiUrl + '/stripe/auth', {}, true);
  // }
  public checkout(data) {
    return this.post(this.apiUrl + '/checkout', data, true);
  }
  // Checkout | Uusi tilaus päättyy

  // Käyttäjän tilaukset
  public getlastOrder() {
    return this.get(this.apiUrl + '/latest_order', {}, true);
  }
  public getAllOrders() {
    return this.get(this.apiUrl + '/all_orders', {}, true);
  }
  public getTrackableOrders() {
    return this.get(this.apiUrl + '/track_orders', {}, true);
  }
  // Käyttäjän tilaukset päättyy

  // Käyttäjän toiminnot
  // Suosikit
  public getFavorites() {
    return this.get(this.apiUrl + '/favorites', {}, true);
  }
  public addToFavorites(data) {
    return this.post(this.apiUrl + '/add_favorite', data, true);
  }
  public removeFromFavorites(data) {
    return this.post(this.apiUrl + '/remove_favorite', data, true);
  }
  // Viestit
  public getThreads() {
    return this.get(this.apiUrl + '/threads', {}, true);
  }
  public setMessageRead(data) {
    return this.post(this.apiUrl + '/update_thread', data, true);
  }
  public sendMessage(data) {
    return this.post(this.apiUrl + '/send_message', data, true);
  }
  // Käyttäjän toiminnot päättyy

  // Käyttäjä tapahtumat
  public getUserDetails() {
    return this.get(this.apiUrl + '/user', {}, true);
  }
  public updateUserDetails(data) {
    return this.post(this.apiUrl + '/update_user', data, true);
  }
  public changeUserPassword(data) {
    return this.post(this.apiUrl + '/change-password', data, true);
  }
  public login(data) {
    return this.post(this.apiUrl + '/login', data);
  }
  public logout() {
    return this.post(this.apiUrl + '/logout', {}, true);
  }

  // WK
  // public forgotPassword(data) {
  //   return this.post(this.apiUrl + '/user/forgot-password', data);
  // }
  // public forgotPasswordReset(data) {
  //   return this.post(this.apiUrl + '/user/forgot-password/change', data);
  // }
  //
  // KL
  public sendPasswordReset(phoneNumber) {
    // const req = this.createHttpPostHeader({ phone_number: phoneNumber }, false);
    // return this.post(this.userResetPasswordUrl, req);
    return this.post(this.apiUrl + '/user/reset', { phone_number: phoneNumber });
  }
  public sendPasswordResetCode(data) {
    // const req = this.createHttpPostHeader(args, false);
    // return this.post(this.userResetPasswordCodeUrl, req);
    return this.post(this.apiUrl + '/user/reset/code', data);
  }

  // public registerCandidate(data) {
  //   return this.post(this.apiUrl + '/register/candidate', data);
  // }
  // public registerUser(data) {
  //   return this.post(this.apiUrl + '/register', data);
  // }
  // public newCodeRequest(data) {
  //   return this.post(this.apiUrl + '/register/candidate/new-code', data);
  // }
  // Käyttäjä tapahtumat päättyy

  // Rekisteröinti
  public createUserCandidate(data) {
    // const req = this.createHttpPostHeader(data, false);
    // return this.post(this.userCandidateCreateUrl, req);
    return this.post(this.apiUrl + '/register/create', data);
  }
  // private userCandidateSaveUrl = this.url + '/user/create/save';
  public saveUserCandidate(data) {
  //   const req = this.createHttpPostHeader(data, false);
  //   return this.post(this.userCandidateSaveUrl, req);
    return this.post(this.apiUrl + '/register/save', data);
  }

  // private userCandidateVerifyUrl = this.url + '/user/create/verify';
  public verifyCandidateAsUser(data) {
  //   const req = this.createHttpPostHeader(data, false);
  //   return this.post(this.userCandidateVerifyUrl, req);
    // return this.post(this.apiUrl + '/register/verify', data, true);
    return this.post(this.apiUrl + '/register/verify', data);
  }

  //

  private get(url, data?, asLoggedInUser?) {
    let headers = {};
    let acctoken;

    try {
      if (asLoggedInUser) {
        if (!this.accessToken) {
          acctoken = this.user.getApiToken();
          if (!acctoken) {
            console.error('auth-required api request without access token', url, data);
            return Promise.reject(false);
          }
          headers = {
            Authorization: 'Bearer ' + acctoken,
          };
          // console.log(acctoken);
        } else {
          headers = {
            Authorization: 'Bearer ' + this.accessToken,
          };
          // console.log(this.accessToken);
        }
      }
    } catch (e) {
      console.error(e);
      console.log('juma');
    }

    // return new Promise((resolve, reject) => {
    // console.log('plöö ' + headers);
    // console.log(JSON.stringify(headers));
    // console.table(headers);
    return this.http.get(url, { headers, params: data || {} }).pipe(timeout(10000), catchError(e => Promise.reject(e))).toPromise();
  }
  private post(url, data, asLoggedInUser?) {
    let headers = {};
    let acctoken;

    if (asLoggedInUser) {
      if (!this.accessToken) {
        acctoken = this.user.getApiToken();
        if (!acctoken) {
          console.error('auth-required api request without access token', url, data);
          return Promise.reject(false);
        }
        headers = {
          Authorization: 'Bearer ' + acctoken,
        };
      } else {
        headers = {
          Authorization: 'Bearer ' + this.accessToken,
        };
      }

      
    }

    return this.http.post(url, data, { headers }).pipe(timeout(10000), catchError(e => Promise.reject(e))).toPromise();
  }

  public setAccessToken(token) {
    this.accessToken = token;
  }

  public hasApiToken() {
    return this.accessToken !== null;
  }

  public loadUserInfo() {
    try {
      // const req = this.createHttpGetHeader(true);
      // return this.get(this.userInfoUrl + '?' + req.url, req);
      return this.getUserDetails();
      // {
      //   return this.get(this.apiUrl + '/user', {}, true);
      // }
    } catch (e) {
      console.error(e);
      console.log('Vittu');
      return Promise.reject(e); // palauttaa unauthorized?
    }
  }

  public updateDeviceData(details) {
    try {
      // const req = this.createHttpPostHeader(details, true);
      return this.post(this.apiUrl + '/device/update', details, true);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  public registerDeviceData(details) {
    try {
      // const req = this.createHttpPostHeader(details, false);
      return this.post(this.apiUrl + '/device/register', details);
    } catch (e) {
      console.error(e);
      return Promise.reject(e);
    }
  }

  private createHttpPostHeader(data, includeAccessToken) {
    const accessToken = includeAccessToken !== undefined && includeAccessToken !== false ? this.user.getApiToken() : '';
    const options: any = { headers: {} };
    if (includeAccessToken) { options.headers.Authorization = 'Bearer ' + accessToken; }

    return {
      data,
      options,
    };
  }
  private createHttpGetHeader(includeAccessToken) {
    let accessToken;

    if (includeAccessToken !== undefined && includeAccessToken !== false) {
      accessToken = this.user.getApiToken();
      // if (!accessToken) { throw new AuthExpiredError('Api token missing'); }
    }

    const options: any = { headers: {} };
    const url = '';

    if (includeAccessToken) { options.headers.Authorization = 'Bearer ' + accessToken; }

    return {
      url,
      options,
    };
  }

  public deviceUnlink() {
    try {
      // const req = this.createHttpGetHeader(true);
      return this.post(this.apiUrl + '/device/unlink', {}, true);
    } catch (e) {
      return Promise.reject(e);
    }
  }

}
