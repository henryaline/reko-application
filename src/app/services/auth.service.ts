import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ApiDataService } from './api-data.service';
import { AppDataService } from './app-data.service';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { REKOUser } from './user';
import { EventsService } from './events.service';
import { Plugins } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any;
  private loggedIn = false;
  private watch = null;
  // private user: User;
  private sentPushToken = false;

  constructor(  private user: REKOUser, private http: HttpClient, private storage: NativeStorage,
                private apid: ApiDataService, private appData: AppDataService, private events: EventsService) { }

  sendDeviceToken() {
    Plugins.PushNotifications.requestPermission().then(permission => {
      if (permission.granted) {
        Plugins.PushNotifications.addListener('registration', token => {
          console.log(token.value);
        });
        Plugins.PushNotifications.addListener('pushNotificationReceived', notification => {
          console.log(notification);
        });
        Plugins.PushNotifications.register().then(() => {
          console.log('pushnotificatins registered');
        });
      }
    }).catch(err => console.error(err));
  }
  // Checkout | Uusi tilaus
  checkout(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.checkout(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }

  // Ostoskori
  addCart(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.addToCart(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }
  rmOneFromCart(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.removeOneFromCart(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }
  rmFromCart(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.removeFromCart(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }

  // Favs
  addFavs(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.addToFavorites(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }
  rmFromFavs(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.removeFromFavorites(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }
  // Favs end

  // Msgs
  sendMsg(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.sendMessage(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }

  // Käyttäjän tietojen päivitys
  updateUser(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.updateUserDetails(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }

  changePassword(values) {
    return new Promise((resolve, reject) => {
      this.apid.getUserDetails().then(() => {
        return this.apid.changeUserPassword(values);
      }).then(() => {
        resolve(true);
      }).catch(err => reject(err));
    });
  }

  check() {
    if (this.apid.hasApiToken()) {
      return Promise.resolve(true);
    } else {
      return new Promise((resolve, reject) => {
        this.appData.getFromStorage('access_token').then(token => {
          if (token) {
            this.apid.setAccessToken(token);
            resolve(true);
          } else {
            reject(false);
          }
        }).catch(err => {
          console.error(err);
          reject(false);
        });
      });
    }
  }
  public getUser() {
    return this.user;
  }

  // load(reset?) {
  //   return new Promise((resolve, reject) => {
  //     // if (!reset && this.appData.hasVariable('api_user')) {
  //     //   return resolve(this.appData.getVariable('api_user'));
  //     // }

  //     this.apid.getUserDetails().then(data => {
  //       this.saveUserDetails(data);

  //       // if (!reset) {
  //       //   this.events.publish('user-login-status-changed', true);
  //       // }

  //       resolve(data);
  //     }).catch(err => reject(err));
  //   });
  // }
  public load() {
    return new Promise((resolve, reject) => {
      this.apid.loadUserInfo().then((data: any) => {
        console.log('loadUser läpi');
        this.setDataFromServer(data, false);
        this.loggedIn = true;
        this.events.publish('auth:changed', true);

        if (!this.watch) {
          this.watch = setInterval(() => {
            if (!this.loggedIn) {
              clearInterval(this.watch);
              this.watch = null;
            } else {
              this.check();
            }
          }, 30000); // 5 * 60 sek * 1000
        }

        resolve(data);
      }).catch((err) => {
        console.log(err); // Ongelma eka
        this.logout();
        reject(err);
      });
    });
  }

  login(values) {
    return new Promise((resolve, reject) => {
      this.apid.login(values).then((data: any) => {
        this.saveUserDetails(data);
        // this.events.publish('user-login-status-changed', true);
        this.sendDeviceToken();
        this.deviceLinking();
        resolve(true);
      }).catch(err => {
        console.log('virhe' + err);
        reject(err);
      });
    });
  }

  logout() {
    return new Promise(resolve => {
      this.apid.deviceUnlink().then((data) => { }).catch((err) => { console.log(err); });
      this.apid.setAccessToken(null);
      // this.events.publish('user-login-status-changed', false);

      this.appData.clearStorage().then(() => {
        // const fcm = this.appData.getVariable('fcm');

        // if (fcm) {
        //   fcm.unsubscribeFrom({ topic: 'all' }).then(() => { }).catch(err => console.error(err));
        //   this.fcmAlreadyEnabled = false;
        // }

        return Promise.resolve();
      }).then(() => { }).catch(e => console.error(e)).finally(() => {
        resolve(true);
      });
    });
  }

  // public registerCandidate(data) {
  //   return new Promise((resolve, reject) => {
  //     this.apid.registerCandidate(data).then((data: any) => {
  //       if (Object.keys(data).find(i => i === 'access_token')) {
  //         this.saveUserDetails(data);
  //       }

  //       resolve(data);
  //     }).catch(err => reject(err));
  //   });
  // }

  // public registerUser(data) {
  //   return new Promise((resolve, reject) => {
  //     this.apid.registerUser(data).then((userData: any) => {
  //       this.saveUserDetails(userData);

  //       resolve(userData);
  //     }).catch((err) => {
  //       reject(err);
  //     });
  //   });
  // }

  public saveUserDetails(data) {
    if (data.access_token) {
      this.apid.setAccessToken(data.access_token);
      this.appData.setToStorage('access_token', data.access_token);
      delete data.access_token;
    }

    // try {
    //   if (!this.fcmAlreadyEnabled) {
    //     LocalNotifications.areEnabled().then(enabled => {
    //       const fcm = this.appData.getVariable('fcm');
    //       const areEnabled = enabled.value;

    //       if (fcm && areEnabled) {
    //         // if (data.marketing.app && areEnabled) {
    //         fcm.subscribeTo({ topic: 'all' }).then(() => console.log('subscribed to topic all')).catch(e => console.error(e));
    //         this.fcmAlreadyEnabled = true;
    //         // } else {
    //         //  fcm.unsubscribeFrom({ topic: 'all' }).then(() => console.log('unsubsribed from topic all')).catch(e => console.error(e));
    //         // }
    //       }
    //     }).catch(err => console.error(err));
    //   }
    // } catch (err) {
    //   console.error('catched error', err);
    // }

    // this.appData.setVariable('api_user', data);
    this.appData.setToStorage('api_user', data);
  }

  public getUserDetails() {
    // return this.appData.getVariable('api_user');
    return this.appData.getFromStorage('api_user');
  }
  // getCart() {
  //   // return this.apid.getCartItems();
  //   return new Promise((resolve, reject) => {
  //     // this.apid.getUserDetails().then(() => {
  //     this.apid.getCartItems().then(() => {
  //       // this.apid.getCartItems();
  //       // return this.apid.getCartItems();
  //       resolve(true);
  //     }).catch(err => reject(err));
  //   });
  // }

  public setCandidateDataFromServer(data) {
    this.setUserDetails(data, false);
  }
  public createCandidateUser(data) {
    return new Promise((resolve, reject) => {
      this.apid.createUserCandidate(data).then(candidateData => {
        this.setCandidateDataFromServer(candidateData);
        resolve(true);
      }).catch((err) => {
        reject(err);
      });
    });
  }
  public saveCandidateUser(data) {
    data.candidate_token = this.user.getCandidateToken();

    return new Promise((resolve, reject) => {
      this.apid.saveUserCandidate(data).then((candidateData) => {
        this.setCandidateDataFromServer(candidateData);
        resolve(true);
      }).catch((err) => {
        reject(err);
      });
    });
  }
  public verifyCandidateAsUser(code) {
    const data: any = {
      code,
      candidate_token: this.user.getCandidateToken(),
    };
    console.log(data);

    return new Promise((resolve, reject) => {
      this.apid.verifyCandidateAsUser(data).then((candidateData: any) => {
        console.log(this.user);
        console.log(candidateData);
        this.user.setApiToken(candidateData.access_token);

        this.load().then((userData) => { // Ongelmakohta
          this.sendDeviceToken();
          console.log(userData);
          const dta: any = userData;
          dta.access_token = candidateData.access_token;
          resolve(userData);
        }).catch((err) => {
          console.log(err); // Palauttaa tämän toka
          reject(err);
        });
      }).catch((err) => {
        console.log(err);
        reject(err);
      });
    });
  }

  private saveUser() {
    const data = {};

    for (const i in this.user) {
      if (typeof (this.user[i]) === 'function') {
        continue;
      } else {
        data[i] = this.user[i];
      }
    }

    this.appData.setToStorage('user', data);
  }

  public setUserDetails(data, save) {
    // tslint:disable-next-line:forin
    for (const i in data) {
      this.user[i] = data[i];
    }

    if (save) {
      this.saveUser();
    }
  }
  public setDataFromServer(data: any, save = true) {
    this.setUserDetails(data.user, false);

    if (save) {
      this.saveUser();
    }
  }

  public isAuthenticated() {
    return this.loggedIn;
  }

  public deviceLinking() {
    this.appData.getFromStorage('deviceInfo', null).then((deviceData: any) => {
      if (deviceData !== null) {
        deviceData.user_id = this.user.id;
        this.apid.updateDeviceData(deviceData).then((responseData) => {
          this.appData.setToStorage('deviceInfo', deviceData);
        }).catch((err) => {
          console.log('couldnt send reg device upd');
          console.log(err);
        });
      }
    }).catch((err) => {
      console.log('couldnt reg device');
      console.log(err);
    });
  }

}
