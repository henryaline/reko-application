import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  private variables: any;


  constructor(private storage: Storage) { }

  setToStorage(key, val) {
    return this.storage.set(key, val);
  }

  getFromStorage(key, defaultValue?) {
    return new Promise((resolve, reject) => {
      this.storage.keys().then(keys => {
        if (keys.findIndex(k => k === key) >= 0) {
          this.storage.get(key).then(data => resolve(data)).catch(err => resolve(defaultValue));
        } else {
          resolve(defaultValue);
        }
      }).catch(err => {
        console.error(err);
        resolve(defaultValue);
      });
    });
  }

  clearStorage() {
    // this.variables = {
    //   fcm: this.fcm
    // };
    return this.storage.clear();
  }

  // setVariable(name, val) {
  //   this.variables[name] = val;
  // }

  // getVariable(name, def?) {
  //   if (this.hasVariable(name)) {
  //     return this.variables[name];
  //   }

  //   return def;
  // }

  // hasVariable(name) {
  //   return Object.keys(this.variables).find(s => s === name) !== undefined;
  // }
}
