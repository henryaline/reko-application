import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class REKOUser extends User {
    // tslint:disable-next-line:variable-name
    public api_token: string = null;
    // tslint:disable-next-line:variable-name
    public candidate_token: string = null;

    constructor() {
        super();
    }

    public getApiToken() {
        return this.api_token;
    }

    public setApiToken(token) {
        this.api_token = token;
    }

    public getCandidateToken() {
        return this.candidate_token;
    }

    public setCandidateToken(token) {
        this.candidate_token = token;
    }


    public logout() {
        for (const i in this) {
            if (typeof (this[i]) === 'function') {
                continue;
            } else {
                this[i] = null;
            }
        }
    }
}
