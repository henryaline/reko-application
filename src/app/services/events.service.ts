import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private subjects: { topic, handler }[] = [];

  subscribe(topic, handler) {
    const obj = { topic, handler };
    this.subjects.push(obj);

    return obj;
  }

  publish(topic, ...val) {
    this.subjects.filter(s => s.topic === topic).forEach(subject => {
      subject.handler(...val);
    });
  }

  unsubscribeObject(obj) {
    this.subjects = this.subjects.filter(s => s !== obj);
  }

  unsubscribe(topic, handler?) {
    this.subjects = this.subjects.filter(s => s.topic !== topic && (!handler || s.handler !== handler));
  }

  hasSubscriptions(topic) {
    return this.subjects.find(t => t.topic === topic) !== undefined;
  }
}
